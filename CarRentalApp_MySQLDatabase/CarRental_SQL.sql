CREATE DATABASE  IF NOT EXISTS `teamcanadacarrental` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `teamcanadacarrental`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: teamcanadacarrental
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `age`
--

DROP TABLE IF EXISTS `age`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `age` (
  `AGE_ID` int(2) NOT NULL,
  `AGE_SET` varchar(10) NOT NULL,
  PRIMARY KEY (`AGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `age`
--

LOCK TABLES `age` WRITE;
/*!40000 ALTER TABLE `age` DISABLE KEYS */;
INSERT INTO `age` VALUES (1,'25-30'),(2,'30-35'),(3,'35-40'),(4,'40-45'),(5,'45-50'),(6,'50-55');
/*!40000 ALTER TABLE `age` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization`
--

DROP TABLE IF EXISTS `authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorization` (
  `LOGIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN_NAME` varchar(30) NOT NULL,
  `LOGIN_PASSWORD` varchar(30) NOT NULL,
  PRIMARY KEY (`LOGIN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorization`
--

LOCK TABLES `authorization` WRITE;
/*!40000 ALTER TABLE `authorization` DISABLE KEYS */;
INSERT INTO `authorization` VALUES (1,'admin','humble');
/*!40000 ALTER TABLE `authorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car` (
  `VIN` char(17) NOT NULL,
  `COLOR` varchar(20) NOT NULL,
  `MILES` int(11) NOT NULL,
  `PRODUCTION_YEAR` int(11) NOT NULL,
  `PLATE_NO` char(8) NOT NULL,
  `MODEL_ID` int(5) NOT NULL,
  `CAR_LOCATION` int(3) NOT NULL,
  `AVL_STATUS` char(1) NOT NULL,
  `cat_id` int(3) NOT NULL,
  PRIMARY KEY (`VIN`),
  KEY `FK_CAR_CONST1` (`MODEL_ID`),
  KEY `FK_CAR_CONST2` (`CAR_LOCATION`),
  KEY `fk_car_category_cat_id` (`cat_id`),
  CONSTRAINT `FK_CAR_CONST1` FOREIGN KEY (`MODEL_ID`) REFERENCES `model` (`MODEL_ID`),
  CONSTRAINT `FK_CAR_CONST2` FOREIGN KEY (`CAR_LOCATION`) REFERENCES `location` (`LOC_ID`),
  CONSTRAINT `fk_car_category_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `car_category` (`CAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES ('1B3CB0HA8BD119534','WHITE',11787,2016,'VNCV999X',10004,104,'N',111),('1B3CB9HA8BD119534','WHITE',11787,2016,'VNCV999X',10004,104,'N',111),('1FAFP52U83A256305','RED',2678,2009,'VNCE127C',10003,104,'Y',444),('1FDES14G9DHA32048','WHITE',11787,2016,'YLKN777F',10012,106,'Y',333),('1FDNK64C4RVA38949','WHITE',11787,2016,'CLGZ999X',10004,105,'Y',444),('1FDYW90T5MVA05140','WHITE',11787,2016,'HFX747F',10007,102,'Y',777),('1FMDU35P7VUD46099','BLACK',10678,2010,'PEIZ847A',10001,101,'Y',111),('1FTHX25G1SKB29210','SILVER',6789,2016,'TRNZ119A',10002,103,'Y',444),('1FTNS14L78DB52529','BLACK',10678,2010,'HFXR847A',10001,102,'Y',444),('1FTNX20536EC77908','GREY',2019,2014,'CLGY159S',10005,105,'Y',222),('1FZNX20536EC77908','GREY',2019,2014,'CLGY159S',10005,105,'Y',222),('1G4AJ3541DH997556','BLACK',26007,2013,'YLKN790G',10005,106,'Y',999),('1G4JT69P6FK458200','RED',2678,2009,'TRNT127C',10003,103,'Y',111),('1G4JT69P6FK458260','RED',2678,2009,'TRNT127C',10003,103,'Y',555),('1G6HA89611GC99534','WHITE',11787,2016,'YLWK777F',10006,106,'Y',222),('1G7HA89611GC99534','WHITE',11787,2016,'YLWK777F',10006,106,'Y',222),('1GZGK24F6RE518579','RED',2678,2009,'TRNT677C',10009,103,'Y',333),('1HGFA15249L016870','SILVER',6789,2016,'HFXZ119A',10008,102,'Y',222),('1HGFA15249L016878','SILVER',6789,2016,'HFXZ119A',10008,102,'Y',222),('1HTMMAAM44H557790','BLACK',2666,2013,'TRNS790G',10008,103,'Y',666),('1XP5DR9X9TN483379','BLACK',2666,2013,'PEIY790G',10013,101,'Y',444),('2G1WB58K979348325','GREY',2019,2014,'CLGE179P',10011,105,'Y',333),('2G4CR3DG1BHS17418','BLACK',10678,2010,'PEIZ847A',10007,101,'Y',222),('2G4CR3DG1BHS17419','BLACK',10678,2010,'PEIZ847A',10007,101,'Y',222),('3B7HA18N22G612785','SILVER',6789,2016,'HFXZ119A',10002,102,'Y',111),('3GYT4NEF7DG252530','GREY',2019,2014,'PEID159S',10006,101,'Y',888),('5J6YH27634L034126','WHITE',11787,2016,'VNCZ189F',10010,104,'Y',333);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_category`
--

DROP TABLE IF EXISTS `car_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_category` (
  `CAT_ID` int(3) NOT NULL,
  `CAT_DESC` varchar(20) NOT NULL,
  `CAT_AMOUNT` decimal(9,2) NOT NULL,
  PRIMARY KEY (`CAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_category`
--

LOCK TABLES `car_category` WRITE;
/*!40000 ALTER TABLE `car_category` DISABLE KEYS */;
INSERT INTO `car_category` VALUES (111,'Economy',77.50),(222,'Intermediate',82.05),(333,'Standard',101.50),(444,'Full Size',123.55),(555,'Intermediate SUV',141.99),(666,'Standard SUV',150.75),(777,'Van',167.65),(888,'Off-Road',198.65),(999,'Luxury',201.55);
/*!40000 ALTER TABLE `car_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `CUS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUS_FNAME` varchar(20) NOT NULL,
  `CUS_LNAME` varchar(20) NOT NULL,
  `CUS_AGE_ID` int(2) NOT NULL,
  `CUS_EMAIL` varchar(55) NOT NULL,
  `CUS_PHONE` varchar(20) NOT NULL,
  PRIMARY KEY (`CUS_ID`),
  KEY `FK_CUS_CONST1` (`CUS_AGE_ID`),
  CONSTRAINT `FK_CUS_CONST1` FOREIGN KEY (`CUS_AGE_ID`) REFERENCES `age` (`AGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (9,'asok','kalidass',3,'asok.kalidass@gmail.com','90834546t'),(10,'as','klfnf',3,'asok@gmail.com','ddfdlkfh'),(11,'aso','kalidas',4,'asok.kalidass@gmail.com','kal');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facility`
--

DROP TABLE IF EXISTS `facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility` (
  `FACILITY_ID` int(1) NOT NULL,
  `FACILITY_NAME` varchar(20) NOT NULL,
  `FACILITY_AMT` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`FACILITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facility`
--

LOCK TABLES `facility` WRITE;
/*!40000 ALTER TABLE `facility` DISABLE KEYS */;
INSERT INTO `facility` VALUES (1,'silver',20.00),(2,'gold',15.00),(3,'diamond',25.00);
/*!40000 ALTER TABLE `facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `INV_NUM` int(11) NOT NULL AUTO_INCREMENT,
  `BASIC_PRICE` decimal(9,2) NOT NULL,
  `DAYS_NO` int(11) NOT NULL,
  `CUS_ID` int(11) NOT NULL,
  `INSURANCE_AMOUNT` decimal(9,2) NOT NULL,
  `FACILITY_AMOUNT` decimal(9,2) NOT NULL,
  `TAX` decimal(9,2) NOT NULL,
  `TOTAL_PRICE` decimal(9,2) NOT NULL,
  PRIMARY KEY (`INV_NUM`),
  KEY `FK_INVOICE_CONST1` (`CUS_ID`),
  CONSTRAINT `FK_INVOICE_CONST1` FOREIGN KEY (`CUS_ID`) REFERENCES `customer` (`CUS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (11,77.50,1,9,15.50,7.75,25.00,125.75),(12,77.50,1,9,15.50,7.75,25.00,125.75),(13,77.50,1,9,15.50,7.75,25.00,125.75),(14,141.99,1,10,28.40,14.20,25.00,209.59);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `LOC_ID` int(3) NOT NULL,
  `LOC_MNEN` char(3) NOT NULL,
  `LOC_DESC` varchar(50) NOT NULL,
  `LOC_STREET` varchar(30) DEFAULT NULL,
  `LOC_CITY` varchar(15) DEFAULT NULL,
  `LOC_STATE` varchar(2) DEFAULT NULL,
  `LOC_ZIP` varchar(10) DEFAULT NULL,
  `LOC_PHONE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`LOC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (101,'YYG','Charlottetown Airport, Charlottetown','5250 Maple Hills Ave','Charlottetown','PE','C1C1N2','902-484-1001'),(102,'YHZ','Halifax Stanfield International Airport,Halifax','1 Bell Blvd,Enfield','Halifax','NS','B2T1K2','902-456-1991'),(103,'YYZ','Toronto Pearson International Airport','6301 Silver Dart Dr','Mississauga','ON','L5P1B2','902-674-9981'),(104,'YVR','Vancouver International Airport','3211 Grant McConachie Way','Richmond','BC','V7B0A4','902-499-9991'),(105,'YYC','Calgary International Airport,Halifax','2000 Airport Rd NE','Calgary','AB','T2E6Z8','902-456-8888'),(106,'YZF','Yellowknife International Airport','1 Yellowknife Hwy','Yellowknife','NT','X1A3T2','902-674-7777');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer` (
  `MANUF_ID` int(2) NOT NULL,
  `MANUF_NAME` varchar(20) NOT NULL,
  PRIMARY KEY (`MANUF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer`
--

LOCK TABLES `manufacturer` WRITE;
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
INSERT INTO `manufacturer` VALUES (10,'Audi'),(11,'BMW'),(12,'Chevrolet'),(13,'Dodge'),(14,'Ford'),(15,'GMC'),(16,'Hyundai'),(17,'Jeep'),(18,'Toyota'),(19,'Volkswagen');
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model` (
  `MODEL_ID` int(5) NOT NULL,
  `MODEL_NAME` varchar(30) NOT NULL,
  `MODEL_SIZE` varchar(20) NOT NULL,
  `HORSEPOWER` int(3) NOT NULL,
  `MODEL_PRICE` int(5) NOT NULL,
  `CAT_ID` int(3) NOT NULL,
  `MANUF_ID` int(2) NOT NULL,
  PRIMARY KEY (`MODEL_ID`),
  KEY `FK_MODEL_CONST1` (`CAT_ID`),
  KEY `FK_MODEL_CONST2` (`MANUF_ID`),
  CONSTRAINT `FK_MODEL_CONST1` FOREIGN KEY (`CAT_ID`) REFERENCES `car_category` (`CAT_ID`),
  CONSTRAINT `FK_MODEL_CONST2` FOREIGN KEY (`MANUF_ID`) REFERENCES `manufacturer` (`MANUF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` VALUES (10001,'Chevrolet Spark','22010.84',101,11898,111,12),(10002,'Volkswagen Jetta','19343.93',150,19898,111,19),(10003,'Chevrolet Sonic','23066.78',159,12898,222,12),(10004,'Hyundai Sonata','21000.07',163,15898,333,16),(10005,'Chevrolet Impala','25228.74',177,25898,444,12),(10006,'Dodge Durango','29001.27',189,29196,555,13),(10007,'Ford Edge','27343.93',201,31898,666,14),(10008,'Dodge Grand Caravan','28343.93',211,25898,777,13),(10009,'GMC Sierra 1500','25999.18',222,28988,888,15),(10010,'Jeep Wrangler Unlimited','25996',285,28877,888,17),(10011,'Toyota 4Runner','28000.04',277,27898,888,18),(10012,'Audi A4 ','28843.96',249,35999,999,10),(10013,'BMW 328i X Drive','27343.93',251,34898,999,11);
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rental`
--

DROP TABLE IF EXISTS `rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rental` (
  `RENT_NO` int(11) NOT NULL AUTO_INCREMENT,
  `VIN` varchar(30) NOT NULL,
  `INV_NUM` int(11) NOT NULL,
  `PICKUP_LOC` int(11) NOT NULL,
  `DROPOFF_LOC` int(11) NOT NULL,
  `pickup_date` varchar(15) DEFAULT NULL,
  `dropoff_date` varchar(15) DEFAULT NULL,
  `dropoff_time` varchar(20) DEFAULT NULL,
  `pickup_time` varchar(20) DEFAULT NULL,
  `FACILITY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`RENT_NO`),
  KEY `FK_RENTAL_CONST1` (`VIN`),
  KEY `FK_RENTAL_CONST3` (`PICKUP_LOC`),
  KEY `FK_RENTAL_CONST4` (`DROPOFF_LOC`),
  KEY `fk_rental_inv_num` (`INV_NUM`),
  CONSTRAINT `FK_RENTAL_CONST1` FOREIGN KEY (`VIN`) REFERENCES `car` (`VIN`),
  CONSTRAINT `FK_RENTAL_CONST3` FOREIGN KEY (`PICKUP_LOC`) REFERENCES `location` (`LOC_ID`),
  CONSTRAINT `FK_RENTAL_CONST4` FOREIGN KEY (`DROPOFF_LOC`) REFERENCES `location` (`LOC_ID`),
  CONSTRAINT `fk_rental_inv_num` FOREIGN KEY (`INV_NUM`) REFERENCES `invoice` (`INV_NUM`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rental`
--

LOCK TABLES `rental` WRITE;
/*!40000 ALTER TABLE `rental` DISABLE KEYS */;
INSERT INTO `rental` VALUES (2,'1B3CB9HA8BD119534',11,101,104,'4/2/2016','4/2/2017','1:27:53 AM','1:27:53 AM',3),(3,'1B3CB9HA8BD119534',12,101,104,'4/2/2016','4/2/2017','1:27:53 AM','1:27:53 AM',3),(4,'1B3CB9HA8BD119534',13,101,104,'4/2/2016','4/2/2017','1:27:53 AM','1:27:53 AM',3),(5,'1G4JT69P6FK458260',14,102,105,'4/3/2017','4/3/2017','12:28:06 PM','12:28:06 PM',3);
/*!40000 ALTER TABLE `rental` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation` (
  `RES_ID` int(11) NOT NULL AUTO_INCREMENT,
  `res_date` varchar(15) DEFAULT NULL,
  `PICKUP_LOC` int(11) NOT NULL,
  `PICKUP_DATE` varchar(15) NOT NULL,
  `pickup_time` varchar(20) DEFAULT NULL,
  `DROPOFF_LOC` int(11) NOT NULL,
  `DROPOFF_DATE` varchar(15) NOT NULL,
  `dropoff_time` varchar(20) DEFAULT NULL,
  `FACILITY_ID` int(1) NOT NULL,
  `BASIC_PRICE` decimal(9,2) NOT NULL,
  `INSUR_AMOUNT` decimal(9,2) NOT NULL,
  `TAX` decimal(9,2) NOT NULL,
  `FACILITY_AMOUNT` decimal(9,2) NOT NULL,
  `TOTAL_PRICE` decimal(9,2) NOT NULL,
  `CUS_ID` int(11) NOT NULL,
  `CAT_ID` int(3) NOT NULL,
  `curd_Flag` char(1) DEFAULT NULL,
  `vin` char(17) DEFAULT NULL,
  PRIMARY KEY (`RES_ID`),
  KEY `FK_RESERV_CONST1` (`PICKUP_LOC`),
  KEY `FK_RESERV_CONST2` (`DROPOFF_LOC`),
  KEY `FK_RESERV_CONST3` (`CUS_ID`),
  KEY `FK_RESERV_CONST4` (`CAT_ID`),
  KEY `FK_RESERV_CONST5` (`FACILITY_ID`),
  KEY `fk_car_vin` (`vin`),
  CONSTRAINT `FK_RESERV_CONST1` FOREIGN KEY (`PICKUP_LOC`) REFERENCES `location` (`LOC_ID`),
  CONSTRAINT `FK_RESERV_CONST2` FOREIGN KEY (`DROPOFF_LOC`) REFERENCES `location` (`LOC_ID`),
  CONSTRAINT `FK_RESERV_CONST3` FOREIGN KEY (`CUS_ID`) REFERENCES `customer` (`CUS_ID`),
  CONSTRAINT `FK_RESERV_CONST4` FOREIGN KEY (`CAT_ID`) REFERENCES `car_category` (`CAT_ID`),
  CONSTRAINT `FK_RESERV_CONST5` FOREIGN KEY (`FACILITY_ID`) REFERENCES `facility` (`FACILITY_ID`),
  CONSTRAINT `fk_car_vin` FOREIGN KEY (`vin`) REFERENCES `car` (`VIN`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (8,'4/2/2017',101,'4/2/2016','1:27:53 AM',104,'4/2/2017','1:27:53 AM',3,77.50,15.50,25.00,7.75,125.75,9,111,'D','1B3CB9HA8BD119534'),(9,'4/3/2017',102,'4/3/2017','12:28:06 PM',105,'4/3/2017','12:28:06 PM',3,141.99,28.40,25.00,14.20,209.59,10,555,'C','1G4JT69P6FK458260'),(10,'4/3/2017',104,'4/3/2017','3:28:19 PM',104,'4/3/2017','3:28:19 PM',3,141.99,28.40,25.00,14.20,209.59,11,555,'I',NULL);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'teamcanadacarrental'
--

--
-- Dumping routines for database 'teamcanadacarrental'
--
/*!50003 DROP PROCEDURE IF EXISTS `check_car_availability` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_car_availability`(in vehicleCategoryId varchar(10))
BEGIN
select a.vin, b.cat_desc from  car a
join car_category b
on a.cat_id = b.cat_id
where a.avl_status = 'Y'
and b.cat_id = vehicleCategoryId
limit 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_validUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_validUser`(in loginName varchar(30), in userPassword varchar(30))
BEGIN
select count(*) from authorization where login_name = loginName
and login_password = userPassword;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_reservations` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_reservations`(in resid varchar(10), in cusId varchar(10))
BEGIN
update reservation set curd_flag = 'D'
where res_id = resid and cus_id = cusId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `generate_invoice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_invoice`(in reserveId varchar(10), in noOfDays varchar(10))
BEGIN
INSERT INTO INVOICE (BASIC_PRICE, DAYS_NO, CUS_ID, INSURANCE_AMOUNT, FACILITY_AMOUNT, TAX, TOTAL_PRICE)
(SELECT  BASIC_PRICE, noOfDays, CUS_ID, INSUR_AMOUNT, FACILITY_AMOUNT, TAX, TOTAL_PRICE FROM RESERVATION
WHERE RES_ID = reserveId);

insert into rental (vin, inv_num, pickup_loc, dropoff_loc, pickup_date, dropoff_date, 
dropoff_time, pickup_time, facility_id)
select  vin , ( select max(inv_num) from invoice ), pickup_loc, dropoff_loc, pickup_date, dropoff_date, 
dropoff_time, pickup_time, facility_id 
from reservation where res_id = reserveId;

update car set avl_status = 'Y'
where vin = (select vin from reservation where res_id = reserveId );

update reservation set curd_flag = 'C' where res_id = reserveId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_age_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_age_set`()
BEGIN
SELECT AGE_SET FROM AGE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_carCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_carCategory`()
BEGIN
select cat_desc from CAR_CATEGORY;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_dropoff_location` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_dropoff_location`()
BEGIN
SELECT LOC_DESC FROM LOCATION;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_estimatedCost` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_estimatedCost`(IN vehicleCategory varchar(20), 
                             IN facilityName    varchar(10), IN  noOfDays INT)
BEGIN
set @vehicleCategoryId := (SELECT Cat_id from car_category where cat_desc = vehicleCategory);
set @facilityId := (SELECT facility_id from facility where facility_name = facilityName);
set @basic_amt := (SELECT CAT_AMOUNT FROM CAR_CATEGORY where CAT_ID = @vehicleCategoryId)*(noOfDays);
set @insur_amt := (SELECT CAT_AMOUNT FROM CAR_CATEGORY where CAT_ID = @vehicleCategoryId)*(noOfDays)*(0.20);
set @facility_amt := (SELECT FACILITY_AMT FROM FACILITY where FACILITY_ID = @facilityId);
set @tax_amt := (SELECT CAT_AMOUNT FROM CAR_CATEGORY where CAT_ID = @vehicleCategoryId)*(noOfDays)*(0.10);
set @total := (@basic_amt + @insur_amt + @facility_amt + @tax_amt);
SELECT @basic_amt, @insur_amt, @tax_amt, @facility_amt, @total;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_facilities` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_facilities`()
BEGIN
select facility_name from facility;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_location` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_location`()
BEGIN
SELECT LOC_DESC FROM LOCATION;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reservations` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_reservations`()
BEGIN
SELECT RES_ID as ReservationId, vin as carNumber , CONCAT_WS(" ", b.CUS_FNAME , b.CUS_LNAME)  AS CustomerName, (select LOC_DESC from location where loc_id = pickup_loc) as PickUpLocation, PICKUP_DATE as PickUpDate, PICKUP_TIME as PickUpTime, (select LOC_DESC from location where loc_id = dropoff_loc) as DropOffLocation,
DROPOFF_DATE as DropOffDate, DROPOFF_TIME as DropOffTime, BASIC_PRICE as BasicRental, (select facility_name from facility where facility_id = a.FACILITY_ID) as FacilityName, (select facility_amount from facility where facility_id = a.FACILITY_id) as FacilityAmount,
INSUR_AMOUNT as InsuranceAmount, TAX as Taxes,TOTAL_PRICE as Total, b.CUS_ID as CustomerId, a.facility_id as FacilityId, a.cat_id as car_categoryId, a.curd_flag as flag FROM RESERVATION a 
JOIN CUSTOMER b 
on a.CUS_ID = b.CUS_ID
and a.curd_flag in ('I', 'C'); 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reservationsBySearchCriteria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_reservationsBySearchCriteria`(in resId varchar(10), in resvdate varchar(10))
BEGIN
SELECT RES_ID as ReservationId, vin as carNumber ,CONCAT_WS(" ", b.CUS_FNAME , b.CUS_LNAME)  AS CustomerName, (select LOC_DESC from location where loc_id = pickup_loc) as PickUpLocation, PICKUP_DATE as PickUpDate, PICKUP_TIME as PickUpTime, (select LOC_DESC from location where loc_id = dropoff_loc) as DropOffLocation,
DROPOFF_DATE as DropOffDate, DROPOFF_TIME as DropOffTime, BASIC_PRICE as BasicRental, (select facility_name from facility where facility_id = a.FACILITY_ID) as FacilityName, (select facility_amount from facility where facility_id = a.FACILITY_id) as FacilityAmount,
INSUR_AMOUNT as InsuranceAmount, TAX as Taxes,TOTAL_PRICE as Total, b.CUS_ID as CustomerId, a.facility_id as FacilityId, a.cat_id as car_categoryId, a.curd_flag as flag FROM RESERVATION a 
JOIN CUSTOMER b 
on a.CUS_ID = b.CUS_ID
and a.curd_flag = 'I'
where res_id = resId or pickup_date = resvdate;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-05 21:40:39
