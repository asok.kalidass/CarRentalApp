﻿namespace CarRentalApp
{
    partial class CarRentalHomeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPickUpLoc = new System.Windows.Forms.Label();
            this.lblDropOfLoc = new System.Windows.Forms.Label();
            this.lblPickUpDate = new System.Windows.Forms.Label();
            this.lblDropDate = new System.Windows.Forms.Label();
            this.lblPickUpTime = new System.Windows.Forms.Label();
            this.lblDropTime = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblVehicleCategory = new System.Windows.Forms.Label();
            this.btnContinue = new System.Windows.Forms.Button();
            this.ddlPickUpLocation = new System.Windows.Forms.ComboBox();
            this.ddlDropOffLocation = new System.Windows.Forms.ComboBox();
            this.ddlPickUpDatePicker = new System.Windows.Forms.DateTimePicker();
            this.ddlPickeUpTimePicker = new System.Windows.Forms.DateTimePicker();
            this.ddlDropOffDatePicker = new System.Windows.Forms.DateTimePicker();
            this.ddlDropOffTime = new System.Windows.Forms.DateTimePicker();
            this.ddlAge = new System.Windows.Forms.ComboBox();
            this.ddlVehicleCategory = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAdmin = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPickUpLoc
            // 
            this.lblPickUpLoc.AutoSize = true;
            this.lblPickUpLoc.Location = new System.Drawing.Point(26, 54);
            this.lblPickUpLoc.Name = "lblPickUpLoc";
            this.lblPickUpLoc.Size = new System.Drawing.Size(93, 13);
            this.lblPickUpLoc.TabIndex = 0;
            this.lblPickUpLoc.Text = "Pick-Up Location*";
            // 
            // lblDropOfLoc
            // 
            this.lblDropOfLoc.AutoSize = true;
            this.lblDropOfLoc.Location = new System.Drawing.Point(26, 151);
            this.lblDropOfLoc.Name = "lblDropOfLoc";
            this.lblDropOfLoc.Size = new System.Drawing.Size(95, 13);
            this.lblDropOfLoc.TabIndex = 1;
            this.lblDropOfLoc.Text = "Drop-Off Location*";
            // 
            // lblPickUpDate
            // 
            this.lblPickUpDate.AutoSize = true;
            this.lblPickUpDate.Location = new System.Drawing.Point(26, 87);
            this.lblPickUpDate.Name = "lblPickUpDate";
            this.lblPickUpDate.Size = new System.Drawing.Size(75, 13);
            this.lblPickUpDate.TabIndex = 2;
            this.lblPickUpDate.Text = "Pick-Up Date*";
            // 
            // lblDropDate
            // 
            this.lblDropDate.AutoSize = true;
            this.lblDropDate.Location = new System.Drawing.Point(26, 180);
            this.lblDropDate.Name = "lblDropDate";
            this.lblDropDate.Size = new System.Drawing.Size(77, 13);
            this.lblDropDate.TabIndex = 3;
            this.lblDropDate.Text = "Drop-Off Date*";
            // 
            // lblPickUpTime
            // 
            this.lblPickUpTime.AutoSize = true;
            this.lblPickUpTime.Location = new System.Drawing.Point(26, 116);
            this.lblPickUpTime.Name = "lblPickUpTime";
            this.lblPickUpTime.Size = new System.Drawing.Size(75, 13);
            this.lblPickUpTime.TabIndex = 4;
            this.lblPickUpTime.Text = "Pick-Up Time*";
            // 
            // lblDropTime
            // 
            this.lblDropTime.AutoSize = true;
            this.lblDropTime.Location = new System.Drawing.Point(26, 208);
            this.lblDropTime.Name = "lblDropTime";
            this.lblDropTime.Size = new System.Drawing.Size(77, 13);
            this.lblDropTime.TabIndex = 5;
            this.lblDropTime.Text = "Drop-Off Time*";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(26, 247);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(30, 13);
            this.lblAge.TabIndex = 6;
            this.lblAge.Text = "Age*";
            this.lblAge.Click += new System.EventHandler(this.lblAge_Click);
            // 
            // lblVehicleCategory
            // 
            this.lblVehicleCategory.AutoSize = true;
            this.lblVehicleCategory.Location = new System.Drawing.Point(26, 277);
            this.lblVehicleCategory.Name = "lblVehicleCategory";
            this.lblVehicleCategory.Size = new System.Drawing.Size(91, 13);
            this.lblVehicleCategory.TabIndex = 7;
            this.lblVehicleCategory.Text = "Vehicle Category*";
            // 
            // btnContinue
            // 
            this.btnContinue.Location = new System.Drawing.Point(389, 332);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(127, 38);
            this.btnContinue.TabIndex = 9;
            this.btnContinue.Text = "Continue";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // ddlPickUpLocation
            // 
            this.ddlPickUpLocation.FormattingEnabled = true;
            this.ddlPickUpLocation.Location = new System.Drawing.Point(172, 54);
            this.ddlPickUpLocation.Name = "ddlPickUpLocation";
            this.ddlPickUpLocation.Size = new System.Drawing.Size(174, 21);
            this.ddlPickUpLocation.TabIndex = 1;
            // 
            // ddlDropOffLocation
            // 
            this.ddlDropOffLocation.FormattingEnabled = true;
            this.ddlDropOffLocation.Location = new System.Drawing.Point(172, 143);
            this.ddlDropOffLocation.Name = "ddlDropOffLocation";
            this.ddlDropOffLocation.Size = new System.Drawing.Size(174, 21);
            this.ddlDropOffLocation.TabIndex = 4;
            // 
            // ddlPickUpDatePicker
            // 
            this.ddlPickUpDatePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Inch, ((byte)(0)));
            this.ddlPickUpDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ddlPickUpDatePicker.Location = new System.Drawing.Point(172, 82);
            this.ddlPickUpDatePicker.Name = "ddlPickUpDatePicker";
            this.ddlPickUpDatePicker.Size = new System.Drawing.Size(121, 20);
            this.ddlPickUpDatePicker.TabIndex = 2;
            this.ddlPickUpDatePicker.ValueChanged += new System.EventHandler(this.ddlPickUpDatePicker_ValueChanged);
            // 
            // ddlPickeUpTimePicker
            // 
            this.ddlPickeUpTimePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Inch, ((byte)(0)));
            this.ddlPickeUpTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.ddlPickeUpTimePicker.Location = new System.Drawing.Point(171, 108);
            this.ddlPickeUpTimePicker.Name = "ddlPickeUpTimePicker";
            this.ddlPickeUpTimePicker.Size = new System.Drawing.Size(121, 20);
            this.ddlPickeUpTimePicker.TabIndex = 3;
            // 
            // ddlDropOffDatePicker
            // 
            this.ddlDropOffDatePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Inch, ((byte)(0)));
            this.ddlDropOffDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ddlDropOffDatePicker.Location = new System.Drawing.Point(172, 174);
            this.ddlDropOffDatePicker.Name = "ddlDropOffDatePicker";
            this.ddlDropOffDatePicker.Size = new System.Drawing.Size(121, 20);
            this.ddlDropOffDatePicker.TabIndex = 5;
            this.ddlDropOffDatePicker.ValueChanged += new System.EventHandler(this.ddlDropOffDatePicker_ValueChanged);
            // 
            // ddlDropOffTime
            // 
            this.ddlDropOffTime.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Inch, ((byte)(0)));
            this.ddlDropOffTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.ddlDropOffTime.Location = new System.Drawing.Point(172, 200);
            this.ddlDropOffTime.Name = "ddlDropOffTime";
            this.ddlDropOffTime.Size = new System.Drawing.Size(121, 20);
            this.ddlDropOffTime.TabIndex = 6;
            // 
            // ddlAge
            // 
            this.ddlAge.FormattingEnabled = true;
            this.ddlAge.Location = new System.Drawing.Point(172, 239);
            this.ddlAge.Name = "ddlAge";
            this.ddlAge.Size = new System.Drawing.Size(121, 21);
            this.ddlAge.TabIndex = 7;
            // 
            // ddlVehicleCategory
            // 
            this.ddlVehicleCategory.FormattingEnabled = true;
            this.ddlVehicleCategory.Location = new System.Drawing.Point(172, 269);
            this.ddlVehicleCategory.Name = "ddlVehicleCategory";
            this.ddlVehicleCategory.Size = new System.Drawing.Size(121, 21);
            this.ddlVehicleCategory.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(200, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Car Rental Company";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnAdmin
            // 
            this.btnAdmin.Location = new System.Drawing.Point(488, 12);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(55, 20);
            this.btnAdmin.TabIndex = 0;
            this.btnAdmin.Text = "Admin";
            this.btnAdmin.UseVisualStyleBackColor = true;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "* mandatory fields";
            // 
            // CarRentalHomeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 382);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnAdmin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlVehicleCategory);
            this.Controls.Add(this.ddlAge);
            this.Controls.Add(this.ddlDropOffTime);
            this.Controls.Add(this.ddlDropOffDatePicker);
            this.Controls.Add(this.ddlPickeUpTimePicker);
            this.Controls.Add(this.ddlPickUpDatePicker);
            this.Controls.Add(this.ddlDropOffLocation);
            this.Controls.Add(this.ddlPickUpLocation);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.lblVehicleCategory);
            this.Controls.Add(this.lblAge);
            this.Controls.Add(this.lblDropTime);
            this.Controls.Add(this.lblPickUpTime);
            this.Controls.Add(this.lblDropDate);
            this.Controls.Add(this.lblPickUpDate);
            this.Controls.Add(this.lblDropOfLoc);
            this.Controls.Add(this.lblPickUpLoc);
            this.Name = "CarRentalHomeView";
            this.Text = "Car Rental Application V1.0";
            this.Load += new System.EventHandler(this.frmCarRentalAdmin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPickUpLoc;
        private System.Windows.Forms.Label lblDropOfLoc;
        private System.Windows.Forms.Label lblPickUpDate;
        private System.Windows.Forms.Label lblDropDate;
        private System.Windows.Forms.Label lblPickUpTime;
        private System.Windows.Forms.Label lblDropTime;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblVehicleCategory;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.ComboBox ddlPickUpLocation;
        private System.Windows.Forms.ComboBox ddlDropOffLocation;
        private System.Windows.Forms.DateTimePicker ddlPickUpDatePicker;
        private System.Windows.Forms.DateTimePicker ddlPickeUpTimePicker;
        private System.Windows.Forms.DateTimePicker ddlDropOffDatePicker;
        private System.Windows.Forms.DateTimePicker ddlDropOffTime;
        private System.Windows.Forms.ComboBox ddlAge;
        private System.Windows.Forms.ComboBox ddlVehicleCategory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Label label3;
    }
}

