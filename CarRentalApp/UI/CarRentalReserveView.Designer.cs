﻿namespace CarRentalApp.UI
{
    partial class CarRentalReserveView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblheader = new System.Windows.Forms.Label();
            this.lblInsuranceAmount = new System.Windows.Forms.Label();
            this.txtInsurance = new System.Windows.Forms.TextBox();
            this.lblCustomerDetails = new System.Windows.Forms.Label();
            this.lblFName = new System.Windows.Forms.Label();
            this.txtFname = new System.Windows.Forms.TextBox();
            this.lblLName = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblESDAmount = new System.Windows.Forms.Label();
            this.lblRental = new System.Windows.Forms.Label();
            this.txtBasicRental = new System.Windows.Forms.TextBox();
            this.lblTaxes = new System.Windows.Forms.Label();
            this.txtTaxes = new System.Windows.Forms.TextBox();
            this.lblInsurance = new System.Windows.Forms.Label();
            this.txtInsuranceAmount = new System.Windows.Forms.TextBox();
            this.btnReserveNow = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtFacility = new System.Windows.Forms.TextBox();
            this.lblFacilityAmount = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ddlFacilityType = new System.Windows.Forms.ComboBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblEstimatedTotal = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Location = new System.Drawing.Point(12, 21);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(158, 13);
            this.lblheader.TabIndex = 0;
            this.lblheader.Text = "Additional Services / Equipment";
            // 
            // lblInsuranceAmount
            // 
            this.lblInsuranceAmount.AutoSize = true;
            this.lblInsuranceAmount.Location = new System.Drawing.Point(16, 53);
            this.lblInsuranceAmount.Name = "lblInsuranceAmount";
            this.lblInsuranceAmount.Size = new System.Drawing.Size(57, 13);
            this.lblInsuranceAmount.TabIndex = 1;
            this.lblInsuranceAmount.Text = "Insurance ";
            // 
            // txtInsurance
            // 
            this.txtInsurance.Enabled = false;
            this.txtInsurance.Location = new System.Drawing.Point(73, 29);
            this.txtInsurance.Name = "txtInsurance";
            this.txtInsurance.Size = new System.Drawing.Size(143, 20);
            this.txtInsurance.TabIndex = 1;
            // 
            // lblCustomerDetails
            // 
            this.lblCustomerDetails.AutoSize = true;
            this.lblCustomerDetails.Location = new System.Drawing.Point(12, 92);
            this.lblCustomerDetails.Name = "lblCustomerDetails";
            this.lblCustomerDetails.Size = new System.Drawing.Size(86, 13);
            this.lblCustomerDetails.TabIndex = 3;
            this.lblCustomerDetails.Text = "Customer Details";
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(14, 121);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(61, 13);
            this.lblFName.TabIndex = 4;
            this.lblFName.Text = "First Name*";
            // 
            // txtFname
            // 
            this.txtFname.Location = new System.Drawing.Point(88, 118);
            this.txtFname.Name = "txtFname";
            this.txtFname.Size = new System.Drawing.Size(143, 20);
            this.txtFname.TabIndex = 300;
            this.txtFname.TabStop = false;
            // 
            // lblLName
            // 
            this.lblLName.AutoSize = true;
            this.lblLName.Location = new System.Drawing.Point(265, 33);
            this.lblLName.Name = "lblLName";
            this.lblLName.Size = new System.Drawing.Size(62, 13);
            this.lblLName.TabIndex = 6;
            this.lblLName.Text = "Last Name*";
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(349, 29);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(143, 20);
            this.txtLName.TabIndex = 4;
            this.txtLName.TabStop = false;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(15, 175);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(42, 13);
            this.lblPhone.TabIndex = 8;
            this.lblPhone.Text = "Phone*";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(88, 168);
            this.txtPhone.MaxLength = 10;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(143, 20);
            this.txtPhone.TabIndex = 5;
            this.txtPhone.TabStop = false;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(265, 83);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(36, 13);
            this.lblEmail.TabIndex = 10;
            this.lblEmail.Text = "Email*";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(349, 80);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(143, 20);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.TabStop = false;
            // 
            // lblESDAmount
            // 
            this.lblESDAmount.AutoSize = true;
            this.lblESDAmount.Location = new System.Drawing.Point(10, 232);
            this.lblESDAmount.Name = "lblESDAmount";
            this.lblESDAmount.Size = new System.Drawing.Size(92, 13);
            this.lblESDAmount.TabIndex = 12;
            this.lblESDAmount.Text = "Estimated Amount";
            // 
            // lblRental
            // 
            this.lblRental.AutoSize = true;
            this.lblRental.Location = new System.Drawing.Point(14, 268);
            this.lblRental.Name = "lblRental";
            this.lblRental.Size = new System.Drawing.Size(67, 13);
            this.lblRental.TabIndex = 13;
            this.lblRental.Text = "Basic Rental";
            // 
            // txtBasicRental
            // 
            this.txtBasicRental.Enabled = false;
            this.txtBasicRental.Location = new System.Drawing.Point(88, 265);
            this.txtBasicRental.Name = "txtBasicRental";
            this.txtBasicRental.Size = new System.Drawing.Size(143, 20);
            this.txtBasicRental.TabIndex = 14;
            // 
            // lblTaxes
            // 
            this.lblTaxes.AutoSize = true;
            this.lblTaxes.Location = new System.Drawing.Point(14, 310);
            this.lblTaxes.Name = "lblTaxes";
            this.lblTaxes.Size = new System.Drawing.Size(36, 13);
            this.lblTaxes.TabIndex = 15;
            this.lblTaxes.Text = "Taxes";
            // 
            // txtTaxes
            // 
            this.txtTaxes.Enabled = false;
            this.txtTaxes.Location = new System.Drawing.Point(88, 310);
            this.txtTaxes.Name = "txtTaxes";
            this.txtTaxes.Size = new System.Drawing.Size(143, 20);
            this.txtTaxes.TabIndex = 16;
            // 
            // lblInsurance
            // 
            this.lblInsurance.AutoSize = true;
            this.lblInsurance.Location = new System.Drawing.Point(278, 268);
            this.lblInsurance.Name = "lblInsurance";
            this.lblInsurance.Size = new System.Drawing.Size(57, 13);
            this.lblInsurance.TabIndex = 17;
            this.lblInsurance.Text = "Insurance ";
            // 
            // txtInsuranceAmount
            // 
            this.txtInsuranceAmount.Enabled = false;
            this.txtInsuranceAmount.Location = new System.Drawing.Point(350, 33);
            this.txtInsuranceAmount.Name = "txtInsuranceAmount";
            this.txtInsuranceAmount.Size = new System.Drawing.Size(143, 20);
            this.txtInsuranceAmount.TabIndex = 18;
            // 
            // btnReserveNow
            // 
            this.btnReserveNow.Location = new System.Drawing.Point(281, 446);
            this.btnReserveNow.Name = "btnReserveNow";
            this.btnReserveNow.Size = new System.Drawing.Size(122, 37);
            this.btnReserveNow.TabIndex = 7;
            this.btnReserveNow.Text = "Reserve Now";
            this.btnReserveNow.UseVisualStyleBackColor = true;
            this.btnReserveNow.Click += new System.EventHandler(this.btnReserveNow_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblEmail);
            this.groupBox1.Controls.Add(this.lblLName);
            this.groupBox1.Controls.Add(this.txtLName);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Location = new System.Drawing.Point(13, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(539, 124);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "* mandatory fields";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtFacility);
            this.groupBox2.Controls.Add(this.lblFacilityAmount);
            this.groupBox2.Controls.Add(this.txtInsuranceAmount);
            this.groupBox2.Location = new System.Drawing.Point(12, 232);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(539, 107);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "1";
            // 
            // txtFacility
            // 
            this.txtFacility.Enabled = false;
            this.txtFacility.Location = new System.Drawing.Point(350, 71);
            this.txtFacility.Name = "txtFacility";
            this.txtFacility.Size = new System.Drawing.Size(143, 20);
            this.txtFacility.TabIndex = 24;
            // 
            // lblFacilityAmount
            // 
            this.lblFacilityAmount.AutoSize = true;
            this.lblFacilityAmount.Location = new System.Drawing.Point(266, 78);
            this.lblFacilityAmount.Name = "lblFacilityAmount";
            this.lblFacilityAmount.Size = new System.Drawing.Size(39, 13);
            this.lblFacilityAmount.TabIndex = 23;
            this.lblFacilityAmount.Text = "Facility";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.ddlFacilityType);
            this.groupBox3.Controls.Add(this.txtInsurance);
            this.groupBox3.Location = new System.Drawing.Point(15, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(537, 65);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // ddlFacilityType
            // 
            this.ddlFacilityType.FormattingEnabled = true;
            this.ddlFacilityType.Location = new System.Drawing.Point(347, 29);
            this.ddlFacilityType.Name = "ddlFacilityType";
            this.ddlFacilityType.Size = new System.Drawing.Size(143, 21);
            this.ddlFacilityType.TabIndex = 2;
            this.ddlFacilityType.SelectedIndexChanged += new System.EventHandler(this.ddlFacilityType_SelectedIndexChanged);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(244, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(101, 13);
            this.lblTitle.TabIndex = 27;
            this.lblTitle.Text = "Reservation Screen";
            // 
            // lblEstimatedTotal
            // 
            this.lblEstimatedTotal.AutoSize = true;
            this.lblEstimatedTotal.Location = new System.Drawing.Point(17, 363);
            this.lblEstimatedTotal.Name = "lblEstimatedTotal";
            this.lblEstimatedTotal.Size = new System.Drawing.Size(80, 13);
            this.lblEstimatedTotal.TabIndex = 23;
            this.lblEstimatedTotal.Text = "Estimated Total";
            // 
            // txtTotal
            // 
            this.txtTotal.Enabled = false;
            this.txtTotal.Location = new System.Drawing.Point(73, 32);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(143, 20);
            this.txtTotal.TabIndex = 25;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(17, 398);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(31, 13);
            this.lblTotal.TabIndex = 25;
            this.lblTotal.Text = "Total";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtTotal);
            this.groupBox4.Location = new System.Drawing.Point(15, 363);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(536, 65);
            this.groupBox4.TabIndex = 26;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(429, 446);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(122, 37);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(233, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Facility Type*";
            // 
            // CarRentalReserveView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 504);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblEstimatedTotal);
            this.Controls.Add(this.btnReserveNow);
            this.Controls.Add(this.lblInsurance);
            this.Controls.Add(this.txtTaxes);
            this.Controls.Add(this.lblTaxes);
            this.Controls.Add(this.txtBasicRental);
            this.Controls.Add(this.lblRental);
            this.Controls.Add(this.lblESDAmount);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.txtFname);
            this.Controls.Add(this.lblFName);
            this.Controls.Add(this.lblCustomerDetails);
            this.Controls.Add(this.lblInsuranceAmount);
            this.Controls.Add(this.lblheader);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Name = "CarRentalReserveView";
            this.Text = "Car Rental Application V1.0";
            this.Load += new System.EventHandler(this.CarRentalSubmitScreen_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Label lblInsuranceAmount;
        private System.Windows.Forms.TextBox txtInsurance;
        private System.Windows.Forms.Label lblCustomerDetails;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.TextBox txtFname;
        private System.Windows.Forms.Label lblLName;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblESDAmount;
        private System.Windows.Forms.Label lblRental;
        private System.Windows.Forms.TextBox txtBasicRental;
        private System.Windows.Forms.Label lblTaxes;
        private System.Windows.Forms.TextBox txtTaxes;
        private System.Windows.Forms.Label lblInsurance;
        private System.Windows.Forms.TextBox txtInsuranceAmount;
        private System.Windows.Forms.Button btnReserveNow;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtFacility;
        private System.Windows.Forms.Label lblFacilityAmount;
        private System.Windows.Forms.Label lblEstimatedTotal;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox ddlFacilityType;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}