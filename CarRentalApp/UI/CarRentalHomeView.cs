﻿#region "NameSpaces"

using CarRentalApp.BusinessLayer;
using CarRentalApp.UI;
using System;
using System.Windows.Forms;

#endregion

namespace CarRentalApp
{
    /// <summary>
    /// This class represent the home screen
    /// </summary>
    public partial class CarRentalHomeView : Form
    {
        #region "Class Variables"

        //Manager class variable
        IDropDownBinder _ddlBinder;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        public CarRentalHomeView()
        {
            InitializeComponent();
            _ddlBinder = new DropDownBinder();
        }

        #endregion

        #region "Page Events"

        /// <summary>
        /// Triggers on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void frmCarRentalAdmin_Load(object sender, System.EventArgs e)
        {
            //gets and binds car category
            ddlVehicleCategory.DataSource = _ddlBinder.getDropDownValues("carCategory");
            ddlVehicleCategory.DisplayMember = "Value";
            ddlVehicleCategory.ValueMember = "Id";

            //gets and binds pickup locations
            ddlPickUpLocation.DataSource = _ddlBinder.getDropDownValues("locations");
            ddlPickUpLocation.DisplayMember = "Value";
            ddlPickUpLocation.ValueMember = "Id";

            //gets and binds drop off locations
            ddlDropOffLocation.DataSource = _ddlBinder.getDropDownValues("locations");
            ddlDropOffLocation.DisplayMember = "Value";
            ddlDropOffLocation.ValueMember = "Id";

            //gets and binds age
            ddlAge.DataSource = _ddlBinder.getDropDownValues(string.Empty);
            ddlAge.DisplayMember = "Value";
            ddlAge.ValueMember = "Id";

            //sets the default value in dropdowns
            setSelectedIndex();
        }

        /// <summary>
        /// Triggers on continue click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnContinue_Click(object sender, EventArgs e)
        {
            //validation
            if (ddlPickUpLocation.Text.Trim() == string.Empty
            || ddlDropOffLocation.Text.Trim() == string.Empty
            || ddlAge.Text.Trim() == string.Empty
            || ddlVehicleCategory.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Mandatory Fields values are missing. Please select and proceed");
                return;
            }

            if (!(DropOffDatePickerValidation()))
            {
                return;
            }

            if (!(pickUpDatePickerValidation()))
            {
                return;
            }

            this.Hide();
            var carReserveScreen = new CarRentalReserveView(setValue());
            carReserveScreen.Show();
        }

        /// <summary>
        /// Triggers on Admin click 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdmin_Click(object sender, EventArgs e)
        {
            //close the current screen
            this.Hide();
            //navigate user to login screen
            var loginScreen = new Login();
            loginScreen.Show();
        }

        private void lblAge_Click(object sender, EventArgs e) { }

        private void label1_Click(object sender, EventArgs e) { }

        #endregion

        #region "Private Methods"

        /// <summary>
        /// Sets the reservation attributes
        /// </summary>
        /// <returns></returns>
        private Reservation setValue()
        {
            Reservation reserve = new Reservation();
            reserve.Age = ddlAge.Text;
            reserve.DropOffLocation = ddlDropOffLocation.Text;
            reserve.DropOffTime = ddlDropOffTime.Text;
            reserve.DropOffDate = Convert.ToDateTime(ddlDropOffDatePicker.Text.Trim()).ToString("MM/dd/yyyy");
            reserve.PickUpDate = Convert.ToDateTime(ddlPickUpDatePicker.Text.Trim()).ToString("MM/dd/yyyy"); 
            reserve.PickUpLocation = ddlPickUpLocation.Text;
            reserve.PickUpTime = ddlPickeUpTimePicker.Text;
            reserve.VehicleCategory = ddlVehicleCategory.Text;
            return reserve;
        }

        private void setSelectedIndex()
        {
            ddlPickUpLocation.SelectedValue = string.Empty;
            ddlDropOffLocation.SelectedValue = string.Empty;
            ddlAge.SelectedValue = string.Empty;
            ddlVehicleCategory.SelectedValue = string.Empty;
        }

        #endregion

        private void ddlDropOffDatePicker_ValueChanged(object sender, EventArgs e)
        {
            DropOffDatePickerValidation();
        }

        private bool pickUpDatePickerValidation()
        {
            if ((Convert.ToDateTime(ddlPickUpDatePicker.Text.Trim()) < DateTime.Today))
            {
                MessageBox.Show("pick-Off Date should be greater than today's date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void ddlPickUpDatePicker_ValueChanged(object sender, EventArgs e)
        {
            pickUpDatePickerValidation();
        }

        private bool DropOffDatePickerValidation()
        {
            if (Convert.ToDateTime(ddlDropOffDatePicker.Text.Trim()) < Convert.ToDateTime(ddlPickUpDatePicker.Text.Trim()))
            {
                MessageBox.Show("Drop-Off Date should be greater than pick-up date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if ((Convert.ToDateTime(ddlDropOffDatePicker.Text.Trim()) < DateTime.Today))
            {
                MessageBox.Show("Drop-Off Dates should be greater than today's date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
    }
}
