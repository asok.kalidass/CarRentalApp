﻿#region "NameSpaces"

using CarRentalApp.BusinessLayer;
using System;
using System.Windows.Forms;

#endregion

namespace CarRentalApp.UI
{
    /// <summary>
    /// This class is resoinsible for login authorization to admin screen
    /// </summary>
    public partial class Login : Form
    {
        #region "Class Variables"

        IAuthorizationManager _authManager;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        public Login()
        {
            InitializeComponent();
        }

        #endregion

        #region "Page Events"

        /// <summary>
        /// Submit Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            txtInvalidUser.Text = string.Empty;
            _authManager = new AuthorizationManager();
            bool isValidUser = _authManager.isValidUser(txtLoginId.Text.Trim(), txtPassword.Text.Trim());

            if(isValidUser)
            {
                this.Hide();
                var adminScreen = new AdminView();
                adminScreen.Show();
            }
            else
            {
                txtInvalidUser.Text = "User is not authorized to proceed. Please, click cancel to return to home screen";
            }
        }

        /// <summary>
        /// Cancel Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //close the auth screen and take user to home screen
            this.Hide();
            //navigate to home page
            var homeScreen = new CarRentalHomeView();
            homeScreen.Show();
        }

        #endregion
    }
}
