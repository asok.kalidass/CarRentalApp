﻿#region "NameSpaces"

using CarRentalApp.BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

#endregion

namespace CarRentalApp.UI
{
    /// <summary>
    /// This class represent admin view 
    /// </summary>
    public partial class AdminView : Form
    {
        #region "Class Variables"

        IAdminManager _adminManager;

        Boolean isInsert;

        Reservation _reservation;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor to initialize Admin View
        /// </summary>
        public AdminView()
        {
            InitializeComponent();
            this._reservation = new Reservation();
        }

        #endregion

        #region "Admin View Page Events"

        /// <summary>
        /// Screen Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void adminView_Load(object sender, EventArgs e)
        {
            btnClear_Click(sender, e);
            _adminManager = new AdminManager();
            StringBuilder builder = new StringBuilder();                        
           
            dataGridAdmin.DataSource = _adminManager.getReservations().Tables[0];
            this.txtReservationId.Validating += new CancelEventHandler(this.txtReservationId_Validating);
        }

        /// <summary>
        /// Triggers on cell click to update/generate invoice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {                  
            bool isUpdated = false;
            bool isDeleted = false;
            //if click is on new row or header row
            if (e.RowIndex == dataGridAdmin.NewRowIndex || e.RowIndex < 0)
                return;

            setValue(e);
           
            //Check if click is on specific column 
            if (e.ColumnIndex == dataGridAdmin.Columns["dateGridAdminDeleteButton"].Index)
            {
                isDeleted = _adminManager.deleteReservation(_reservation.ReservationId.ToString(), _reservation.CustomerId.ToString()) > 0 ? true : false;

                showMessage(isDeleted, "Record is deleted");

                if (isDeleted) { adminView_Load(sender, e); }
            }

            //Check if click is on specific column 
            if (e.ColumnIndex == dataGridAdmin.Columns["dateGridAdminUpdateButton"].Index)
            {
                if (dataGridAdmin.Rows[e.RowIndex].Cells["Flag"].Value.ToString() == "C")
                {
                    MessageBox.Show("Invoicing is completed. Updation is not allowed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Date validation
                if (Convert.ToDateTime(_reservation.DropOffDate.Trim()) < Convert.ToDateTime(_reservation.PickUpDate.Trim()))
                {
                    MessageBox.Show("Drop-Off Date should be greater than pick-up date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if ((Convert.ToDateTime(_reservation.DropOffDate.Trim()) < DateTime.Today))
                {
                    MessageBox.Show("Drop-Off Date should be greater than today's date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if ((Convert.ToDateTime(_reservation.PickUpDate.Trim()) < DateTime.Today))
                {
                    MessageBox.Show("pick-Off Date should be greater than pick-up date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                bool isCarAvailable = false;
                IList<Entry> vehicle;

                if(!(validateLocations(_reservation))) { return; }
                _reservation.VehicleCategory = _adminManager.getVehicleCategory(_reservation.VehicleCategoryId.ToString())[0].Value.Trim();
                if (!(validateFacility(_reservation))) { return; }

                //Updates the customer information 
                if (isInsert)
                {
                    if (dataGridAdmin.Rows[e.RowIndex].Cells["CarNumber"].Value.ToString() == string.Empty)
                    {
                        //validate weather car is available for the chosen category
                        vehicle = _adminManager.getCategory(_reservation.VehicleCategoryId.ToString());
                        if (vehicle.Count > 0)
                        { 
                        _reservation.Vin = vehicle[0].Value;
                        _reservation.VehicleCategory = vehicle[1].Value;
                        isCarAvailable = _reservation.Vin != string.Empty ? true : false;
                        }
                        if (!(isCarAvailable))
                        {
                            MessageBox.Show("Vehicle is not available to assign to customer", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                    {
                        _reservation.Vin = dataGridAdmin.Rows[e.RowIndex].Cells["CarNumber"].Value.ToString();
                        _reservation.VehicleCategory = _adminManager.getVehicleCategory(_reservation.VehicleCategoryId.ToString())[0].Value.Trim();
                    }
                        isUpdated = _adminManager.updateReservations(refreshEstimatedCost(_reservation)) > 0 ? true : false;
                }
                showMessage(isUpdated, "Record is Updated");
                adminView_Load(sender, e);
            }
            //trigers on invoice button click 
            if (e.ColumnIndex == dataGridAdmin.Columns["dateGridAdminInvoiceButton"].Index)
            {
                if (dataGridAdmin.Rows[e.RowIndex].Cells["Flag"].Value.ToString() == "C")
                {
                    MessageBox.Show("The service has been fullfilled");
                    return;
                }
                else if(dataGridAdmin.Rows[e.RowIndex].Cells["CarNumber"].Value.ToString() == string.Empty)
                {
                    MessageBox.Show("Update the record to assign VIN before invoicing", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
                bool isInvoiceCreated = false;
                //generates the invoice
                isInvoiceCreated = _adminManager.generateInvoice(_reservation.ReservationId.ToString(), calculateDuration().ToString()) > 0 ? true : false;                
                showMessage(isInvoiceCreated, "Invoice is Generated");
                //reloads the grid after invoice generation 
                adminView_Load(sender, e);
                return;
            }
        }

        /// <summary>
        /// Triggers on Cancel Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            var homeScreen = new CarRentalHomeView();
            homeScreen.Show();
        }

        /// <summary>
        /// Triggers on search click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtPickUpDate.Text.Length > 0)
            { 
            try
            {
                Convert.ToDateTime(txtPickUpDate.Text.Trim()).ToString("MM/dd/yyyy");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Date should be in mm/dd/yyyy format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
             }
            if(!(validateNumeric())) { return; }
            if (txtReservationId.Text == string.Empty && txtPickUpDate.Text == string.Empty)
            {
                MessageBox.Show("Please, fill atleast one search criteria", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataTable searchResults = _adminManager.getReservationsBySearchCriteria(txtReservationId.Text.Trim(), Convert.ToDateTime(txtPickUpDate.Text.Trim()).ToString("MM/dd/yyyy")).Tables[0];
           if(searchResults.Rows.Count == 0)
            {
                MessageBox.Show("No records found", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
           dataGridAdmin.DataSource = searchResults;
        }

        /// <summary>
        /// Triggers on clear click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, EventArgs e)
        {
            txtReservationId.Text = string.Empty;
            txtPickUpDate.Text = string.Empty;
        }

        /// <summary>
        /// Applies row coloring for contrack completed records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow row = dataGridAdmin.Rows[e.RowIndex];// get you required index
            if (dataGridAdmin.Rows[e.RowIndex].Cells["Flag"].Value.ToString() == "C")
            {// check the cell value under your specific column and then you can toggle your colors
                row.DefaultCellStyle.BackColor = Color.LightGray;
            }
        }

        #endregion

        #region "Private Methods"

        /// <summary>
        /// Phone Number Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtReservationId_Validating(object sender, CancelEventArgs e)
        {
            validateNumeric();
        }

        private bool validateNumeric()
        {
            Regex validator = new Regex("(3|4|5|6|7|8|9){1}[0-9]{9}");
            string match = validator.Match(txtReservationId.Text).Value.ToString();
            if (txtReservationId.Text.Length > 0)
            {
                if (!(match.Length == 10))
                {
                    MessageBox.Show("Reservation Id should be numeric", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtReservationId.SelectAll();
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Calculates estimation cost
        /// </summary>
        /// <param name="_reservation"></param>
        /// <returns></returns>
        private Reservation refreshEstimatedCost(Reservation _reservation)
        {            
            IList<Entry> estimatedCost = _adminManager.getEstimatedCosts(_reservation.VehicleCategory.Trim(), _reservation.FacilityType.Trim(), calculateDuration().ToString());

            //refresh the reservation details
            if (estimatedCost.Count > 0)
            {
                _reservation.BasicRental = estimatedCost[0].Value.ToString();
                _reservation.InsuranceAmount = estimatedCost[1].Value.ToString();
                _reservation.Taxes = estimatedCost[2].Value.ToString();
                _reservation.FacilityAmount = Convert.ToDouble(estimatedCost[3].Value.ToString());
                _reservation.Total = estimatedCost[4].Value.ToString();
            }
            return _reservation;
        }

        /// <summary>
        /// checks pickup and dropin locations
        /// </summary>
        /// <param name="reservation"></param>
        private bool validateLocations(Reservation reservation)
        {
            bool isValidLocation = false;           

            //validates pickup location
            reservation.PickUpLocationId = _adminManager.validateLocation(reservation.PickUpLocation.Trim());
            isValidLocation = reservation.PickUpLocationId > 0 ? true : false;

            if (!(showMessage(isValidLocation, "PickUp location is found")))
            {
                return false;
            }

            isValidLocation = false;

            //validates drop off location
            reservation.DropOffLocationId = _adminManager.validateLocation(reservation.DropOffLocation.Trim());
            isValidLocation = reservation.DropOffLocationId > 0 ? true : false;

            if (!(showMessage(isValidLocation, "DropOff location is found")))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Caluclate the no of reservation days based on the dates chosen
        /// </summary>
        /// <returns></returns>
        private int calculateDuration()
        {
            //calculate no of days
            return Convert.ToInt32((Convert.ToDateTime(_reservation.PickUpDate) - Convert.ToDateTime(_reservation.DropOffDate)).TotalDays < 1 ? 1 :
                (Convert.ToDateTime(_reservation.PickUpDate) - Convert.ToDateTime(_reservation.DropOffDate)).TotalDays);
        }

        /// <summary>
        /// Validates Facility
        /// </summary>
        /// <param name="reservation"></param>
        private bool validateFacility(Reservation reservation)
        {
            bool isValidFacility = false;

            //validates chosen facility 
            reservation.FacilityId = _adminManager.validateFacility(reservation.FacilityType.Trim());
            isValidFacility = reservation.FacilityId > 0 ? true : false;

            if (!(showMessage(isValidFacility, "Facility is found"))) { return false; }
            refreshEstimatedCost(_reservation);
            return true;
        }

        /// <summary>
        /// Displays corresponding alert to the user
        /// </summary>
        /// <param name="isValid"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool showMessage(bool isValid, string message)
        {
            if(isValid)
            {
                isInsert = true;
                MessageBox.Show(message);
            }
            else
            {
                isInsert = false;
                MessageBox.Show("No " + message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return isValid;
        }

        /// <summary>
        /// Populates the reservation class
        /// </summary>
        /// <param name="e"></param>
        private void setValue(DataGridViewCellEventArgs e)
        {
            _reservation.ReservationId = Convert.ToInt32(dataGridAdmin.Rows[e.RowIndex].Cells[3].Value.ToString());
            _reservation.Vin = dataGridAdmin.Rows[e.RowIndex].Cells[4].Value.ToString();
            _reservation.FirstName = dataGridAdmin.Rows[e.RowIndex].Cells[5].Value.ToString();
            _reservation.PickUpLocation = dataGridAdmin.Rows[e.RowIndex].Cells[6].Value.ToString();
            _reservation.PickUpDate = Convert.ToDateTime(dataGridAdmin.Rows[e.RowIndex].Cells[7].Value.ToString()).ToString("MM/dd/yyyy");
            _reservation.PickUpTime = dataGridAdmin.Rows[e.RowIndex].Cells[8].Value.ToString();
            _reservation.DropOffLocation = dataGridAdmin.Rows[e.RowIndex].Cells[9].Value.ToString();
            _reservation.DropOffDate = Convert.ToDateTime(dataGridAdmin.Rows[e.RowIndex].Cells[10].Value.ToString()).ToString("MM/dd/yyyy");
            _reservation.DropOffTime = dataGridAdmin.Rows[e.RowIndex].Cells[11].Value.ToString();
            _reservation.BasicRental = dataGridAdmin.Rows[e.RowIndex].Cells[12].Value.ToString();
            _reservation.FacilityType = dataGridAdmin.Rows[e.RowIndex].Cells[13].Value.ToString();
            _reservation.FacilityAmount = Convert.ToDouble(dataGridAdmin.Rows[e.RowIndex].Cells[14].Value.ToString());
            _reservation.InsuranceAmount = dataGridAdmin.Rows[e.RowIndex].Cells[15].Value.ToString();
            _reservation.Taxes = dataGridAdmin.Rows[e.RowIndex].Cells[16].Value.ToString();
            _reservation.Total = dataGridAdmin.Rows[e.RowIndex].Cells[17].Value.ToString();
            _reservation.CustomerId = Convert.ToInt32(dataGridAdmin.Rows[e.RowIndex].Cells[18].Value.ToString());
            _reservation.FacilityId = Convert.ToInt32(dataGridAdmin.Rows[e.RowIndex].Cells[19].Value.ToString());
            _reservation.VehicleCategoryId = Convert.ToInt32(dataGridAdmin.Rows[e.RowIndex].Cells[20].Value.ToString());
        }

        #endregion
    }
}
