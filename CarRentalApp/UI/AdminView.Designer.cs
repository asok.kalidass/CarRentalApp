﻿namespace CarRentalApp.UI
{
    partial class AdminView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridAdmin = new System.Windows.Forms.DataGridView();
            this.ReservationId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PickUpLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PickUpDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PickUpTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DropOffLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DropOffDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DropOffTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BasicRental = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FacilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FacilityAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InsuranceAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Taxes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FacilityId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.car_categoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateGridAdminUpdateButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dateGridAdminInvoiceButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dateGridAdminDeleteButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtReservationId = new System.Windows.Forms.TextBox();
            this.lblPickUpDate = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtPickUpDate = new System.Windows.Forms.TextBox();
            this.lblNotify = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAdmin)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridAdmin
            // 
            this.dataGridAdmin.AllowUserToAddRows = false;
            this.dataGridAdmin.AllowUserToDeleteRows = false;
            this.dataGridAdmin.AllowUserToOrderColumns = true;
            this.dataGridAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAdmin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ReservationId,
            this.CarNumber,
            this.CustomerName,
            this.PickUpLocation,
            this.PickUpDate,
            this.PickUpTime,
            this.DropOffLocation,
            this.DropOffDate,
            this.DropOffTime,
            this.BasicRental,
            this.FacilityName,
            this.FacilityAmount,
            this.InsuranceAmount,
            this.Taxes,
            this.Total,
            this.CustomerId,
            this.FacilityId,
            this.car_categoryId,
            this.Flag,
            this.dateGridAdminUpdateButton,
            this.dateGridAdminInvoiceButton,
            this.dateGridAdminDeleteButton});
            this.dataGridAdmin.Location = new System.Drawing.Point(12, 101);
            this.dataGridAdmin.Name = "dataGridAdmin";
            this.dataGridAdmin.Size = new System.Drawing.Size(950, 211);
            this.dataGridAdmin.TabIndex = 0;
            this.dataGridAdmin.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridAdmin.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // ReservationId
            // 
            this.ReservationId.DataPropertyName = "ReservationId";
            this.ReservationId.Frozen = true;
            this.ReservationId.HeaderText = "ReservationId";
            this.ReservationId.Name = "ReservationId";
            this.ReservationId.ReadOnly = true;
            // 
            // CarNumber
            // 
            this.CarNumber.DataPropertyName = "CarNumber";
            this.CarNumber.HeaderText = "CarNumber";
            this.CarNumber.Name = "CarNumber";
            this.CarNumber.ReadOnly = true;
            // 
            // CustomerName
            // 
            this.CustomerName.DataPropertyName = "CustomerName";
            this.CustomerName.HeaderText = "Name";
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.ReadOnly = true;
            // 
            // PickUpLocation
            // 
            this.PickUpLocation.DataPropertyName = "PickUpLocation";
            this.PickUpLocation.HeaderText = "PickUpLocation";
            this.PickUpLocation.Name = "PickUpLocation";
            this.PickUpLocation.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // PickUpDate
            // 
            this.PickUpDate.DataPropertyName = "PickUpDate";
            this.PickUpDate.HeaderText = "PickUpDate";
            this.PickUpDate.Name = "PickUpDate";
            // 
            // PickUpTime
            // 
            this.PickUpTime.DataPropertyName = "PickUpTime";
            this.PickUpTime.HeaderText = "PickUpTime";
            this.PickUpTime.Name = "PickUpTime";
            // 
            // DropOffLocation
            // 
            this.DropOffLocation.DataPropertyName = "DropOffLocation";
            this.DropOffLocation.HeaderText = "DropOffLocation";
            this.DropOffLocation.Name = "DropOffLocation";
            // 
            // DropOffDate
            // 
            this.DropOffDate.DataPropertyName = "DropOffDate";
            this.DropOffDate.HeaderText = "DropOffDate";
            this.DropOffDate.Name = "DropOffDate";
            // 
            // DropOffTime
            // 
            this.DropOffTime.DataPropertyName = "DropOffTime";
            this.DropOffTime.HeaderText = "DropOffTime";
            this.DropOffTime.Name = "DropOffTime";
            // 
            // BasicRental
            // 
            this.BasicRental.DataPropertyName = "BasicRental";
            this.BasicRental.HeaderText = "BasicRental";
            this.BasicRental.Name = "BasicRental";
            this.BasicRental.ReadOnly = true;
            // 
            // FacilityName
            // 
            this.FacilityName.DataPropertyName = "FacilityName";
            this.FacilityName.HeaderText = "FacilityName";
            this.FacilityName.Name = "FacilityName";
            // 
            // FacilityAmount
            // 
            this.FacilityAmount.DataPropertyName = "FacilityAmount";
            this.FacilityAmount.HeaderText = "FacilityAmount";
            this.FacilityAmount.Name = "FacilityAmount";
            this.FacilityAmount.ReadOnly = true;
            // 
            // InsuranceAmount
            // 
            this.InsuranceAmount.DataPropertyName = "InsuranceAmount";
            this.InsuranceAmount.HeaderText = "InsuranceAmount";
            this.InsuranceAmount.Name = "InsuranceAmount";
            this.InsuranceAmount.ReadOnly = true;
            // 
            // Taxes
            // 
            this.Taxes.DataPropertyName = "Taxes";
            this.Taxes.HeaderText = "Taxes";
            this.Taxes.Name = "Taxes";
            this.Taxes.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // CustomerId
            // 
            this.CustomerId.DataPropertyName = "CustomerId";
            this.CustomerId.HeaderText = "CustomerId";
            this.CustomerId.Name = "CustomerId";
            this.CustomerId.ReadOnly = true;
            this.CustomerId.Visible = false;
            // 
            // FacilityId
            // 
            this.FacilityId.DataPropertyName = "FacilityId";
            this.FacilityId.HeaderText = "FacilityId";
            this.FacilityId.Name = "FacilityId";
            this.FacilityId.ReadOnly = true;
            this.FacilityId.Visible = false;
            // 
            // car_categoryId
            // 
            this.car_categoryId.DataPropertyName = "car_categoryId";
            this.car_categoryId.HeaderText = "car_categoryId";
            this.car_categoryId.Name = "car_categoryId";
            this.car_categoryId.ReadOnly = true;
            this.car_categoryId.Visible = false;
            // 
            // Flag
            // 
            this.Flag.DataPropertyName = "Flag";
            this.Flag.HeaderText = "Flag";
            this.Flag.Name = "Flag";
            this.Flag.ReadOnly = true;
            this.Flag.Visible = false;
            // 
            // dateGridAdminUpdateButton
            // 
            this.dateGridAdminUpdateButton.HeaderText = "";
            this.dateGridAdminUpdateButton.Name = "dateGridAdminUpdateButton";
            this.dateGridAdminUpdateButton.Text = "Update";
            this.dateGridAdminUpdateButton.UseColumnTextForButtonValue = true;
            this.dateGridAdminUpdateButton.Width = 45;
            // 
            // dateGridAdminInvoiceButton
            // 
            this.dateGridAdminInvoiceButton.HeaderText = "";
            this.dateGridAdminInvoiceButton.Name = "dateGridAdminInvoiceButton";
            this.dateGridAdminInvoiceButton.Text = "Invoice";
            this.dateGridAdminInvoiceButton.UseColumnTextForButtonValue = true;
            this.dateGridAdminInvoiceButton.Width = 45;
            // 
            // dateGridAdminDeleteButton
            // 
            this.dateGridAdminDeleteButton.HeaderText = "";
            this.dateGridAdminDeleteButton.Name = "dateGridAdminDeleteButton";
            this.dateGridAdminDeleteButton.Text = "Delete";
            this.dateGridAdminDeleteButton.UseColumnTextForButtonValue = true;
            this.dateGridAdminDeleteButton.Width = 45;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(564, 35);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(88, 23);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Reservation Id";
            // 
            // txtReservationId
            // 
            this.txtReservationId.Location = new System.Drawing.Point(91, 36);
            this.txtReservationId.Name = "txtReservationId";
            this.txtReservationId.Size = new System.Drawing.Size(82, 20);
            this.txtReservationId.TabIndex = 1;
            // 
            // lblPickUpDate
            // 
            this.lblPickUpDate.AutoSize = true;
            this.lblPickUpDate.Location = new System.Drawing.Point(195, 41);
            this.lblPickUpDate.Name = "lblPickUpDate";
            this.lblPickUpDate.Size = new System.Drawing.Size(68, 13);
            this.lblPickUpDate.TabIndex = 4;
            this.lblPickUpDate.Text = "PickUp Date";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(797, 35);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(683, 35);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtPickUpDate
            // 
            this.txtPickUpDate.Location = new System.Drawing.Point(280, 34);
            this.txtPickUpDate.Name = "txtPickUpDate";
            this.txtPickUpDate.Size = new System.Drawing.Size(110, 20);
            this.txtPickUpDate.TabIndex = 2;
            // 
            // lblNotify
            // 
            this.lblNotify.AutoSize = true;
            this.lblNotify.Location = new System.Drawing.Point(297, 66);
            this.lblNotify.Name = "lblNotify";
            this.lblNotify.Size = new System.Drawing.Size(71, 13);
            this.lblNotify.TabIndex = 9;
            this.lblNotify.Text = "(mm/dd/yyyy)";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(381, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(196, 13);
            this.lblTitle.TabIndex = 10;
            this.lblTitle.Text = "Admin\'s Reservation/Invoicing Handling";
            // 
            // AdminView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 324);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblNotify);
            this.Controls.Add(this.txtPickUpDate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblPickUpDate);
            this.Controls.Add(this.txtReservationId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.dataGridAdmin);
            this.Name = "AdminView";
            this.Text = "Car Rental Application V1.0";
            this.Load += new System.EventHandler(this.adminView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAdmin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridAdmin;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtReservationId;
        private System.Windows.Forms.Label lblPickUpDate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtPickUpDate;
        private System.Windows.Forms.Label lblNotify;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReservationId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CarNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PickUpLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn PickUpDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PickUpTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DropOffLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn DropOffDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DropOffTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn BasicRental;
        private System.Windows.Forms.DataGridViewTextBoxColumn FacilityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FacilityAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn InsuranceAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Taxes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn FacilityId;
        private System.Windows.Forms.DataGridViewTextBoxColumn car_categoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Flag;
        private System.Windows.Forms.DataGridViewButtonColumn dateGridAdminUpdateButton;
        private System.Windows.Forms.DataGridViewButtonColumn dateGridAdminInvoiceButton;
        private System.Windows.Forms.DataGridViewButtonColumn dateGridAdminDeleteButton;
        private System.Windows.Forms.Label lblTitle;
    }
}