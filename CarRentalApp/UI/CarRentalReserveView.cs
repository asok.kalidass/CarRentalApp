﻿#region "NameSpaces"

using CarRentalApp.BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;

#endregion

namespace CarRentalApp.UI
{
    /// <summary>
    /// This class represents Reserve Screen 
    /// </summary>
    public partial class CarRentalReserveView : Form
    {
        #region "Class Variables"

        //registation class local variable
        private Reservation _reservation;

        //reserve manager variable
        private IRentalReserveManager _rentalManager;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="reservation"></param>
        public CarRentalReserveView(Reservation reservation)
        {
            InitializeComponent();
            this._reservation = reservation;
            this._rentalManager = new RentalReserveManager();
        }

        #endregion

        #region "Page Events"

        /// <summary>
        /// Triggers on Page load 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CarRentalSubmitScreen_Load(object sender, EventArgs e)
        {
            getFacilityType();
            setSelectedIndex();
            txtInsurance.Text = "20% of Basic Rental";
            this.txtEmail.Validating += new CancelEventHandler(this.txtEmail_Validating);
            this.txtPhone.Validating += new CancelEventHandler(this.txtPhone_Validating);
            this.txtFname.Validating += new CancelEventHandler(this.txtFname_Validating);
            this.txtLName.Validating += new CancelEventHandler(this.txtLName_Validating);

            txtFacility.Focus();          
        }

        /// <summary>
        /// Triggers on cancel click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            var homeScreen = new CarRentalHomeView();
            homeScreen.Show();
        }

        /// <summary>
        /// Triggers on on change of facility type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlFacilityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFacilityType.Text != string.Empty)
            {
                //calculate no of days
                double days = (Convert.ToDateTime(_reservation.PickUpDate) - Convert.ToDateTime(_reservation.DropOffDate)).TotalDays < 1 ? 1 :
                    (Convert.ToDateTime(_reservation.PickUpDate) - Convert.ToDateTime(_reservation.DropOffDate)).TotalDays;

                //calculates the estimated cost
                IList<Entry> estimatedCost = _rentalManager.getEstimatedCosts(_reservation.VehicleCategory.Trim(), ddlFacilityType.Text.Trim(), days.ToString());

                txtBasicRental.Text = estimatedCost[0].Value.ToString();
                txtInsuranceAmount.Text = estimatedCost[1].Value.ToString();
                txtTaxes.Text = estimatedCost[2].Value.ToString();
                txtFacility.Text = estimatedCost[3].Value.ToString();
                txtTotal.Text = estimatedCost[4].Value.ToString();

            }
            else
            {
                txtBasicRental.SelectedText = string.Empty;
                txtInsuranceAmount.SelectedText = string.Empty;
                txtTaxes.SelectedText = string.Empty;
                txtFacility.SelectedText = string.Empty;
                txtTotal.SelectedText = string.Empty;
            }
        }

        /// <summary>
        /// Triggers on reserve now event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReserveNow_Click(object sender, EventArgs e)
        {
            //validation
            if (ddlFacilityType.Text == string.Empty
               || txtFname.Text == string.Empty
               || txtLName.Text == string.Empty
               || txtPhone.Text == string.Empty
               || txtEmail.Text == string.Empty)
            {
                MessageBox.Show("Mandatory Fields values are missing. Please select and proceed");
                return;
            }
                      
            bool isReserved = false;
            setValue();
            //get the ids for the chosen type
            _reservation.FacilityId = _rentalManager.getFacilityId(_reservation.FacilityType.Trim());
            _reservation.PickUpLocationId = _rentalManager.getLocationIds(_reservation.PickUpLocation.Trim());
            _reservation.DropOffLocationId = _rentalManager.getLocationIds(_reservation.DropOffLocation.Trim());

            //reserve the reservation
            isReserved = _rentalManager.reserveNow(_reservation) > 0 ? true : false;

            if (isReserved)
            {
                int reserveId = _rentalManager.getReservationId();
                MessageBox.Show("Your Reservation is success. Please note the reservation number: " + reserveId.ToString());
                this.Hide();
                var homeScreen = new CarRentalHomeView();
                homeScreen.Show();
            }
            else
            {
                MessageBox.Show("Your Reservation is not success. Contact customer care");
            }
        }

        /// <summary>
        /// Email Id format validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtEmail_Validating(object sender, CancelEventArgs e)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                 @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                 @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex rEmail = new Regex(strRegex);
            if (txtEmail.Text.Length > 0)
            {
                if (!(rEmail.IsMatch(txtEmail.Text)))
                {
                    MessageBox.Show("Please provide valid email address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.SelectAll();
                }
            }
        }

        /// <summary>
        /// Phone Number Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPhone_Validating(object sender, CancelEventArgs e)
        {
            Regex validator = new Regex("(3|4|5|6|7|8|9){1}[0-9]{9}");
            string match = validator.Match(txtPhone.Text).Value.ToString();
            if (txtPhone.Text.Length > 0)
            {
                if (!(match.Length == 10))
                {
                    MessageBox.Show("Please provide valid  phone number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPhone.SelectAll();
                }
            }
        }

        /// <summary>
        /// First name format validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFname_Validating(object sender, CancelEventArgs e)
        {
            Regex validator = new Regex("^[a-zA-Z][a-zA-Z\\s]+$");
                if (!(validator.IsMatch(txtFname.Text)))
                {
                    MessageBox.Show("Please provide valid  First name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtFname.SelectAll();
                }            
        }

        /// <summary>
        /// Last Name Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLName_Validating(object sender, CancelEventArgs e)
        {
            Regex validator = new Regex("^[a-zA-Z][a-zA-Z\\s]+$");
                if (!(validator.IsMatch(txtLName.Text)))
                {
                    MessageBox.Show("Please provide valid  Last name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtLName.SelectAll();
                }
         }        

        #endregion

        #region "Private Methods"

        /// <summary>
        /// Gets facility type
        /// </summary>
        private void getFacilityType()
        {
            ddlFacilityType.DataSource = _rentalManager.getDropDownValues("facility");
            ddlFacilityType.DisplayMember = "Value";
            ddlFacilityType.ValueMember = "Id";
        }

        /// <summary>
        /// sets resevation attributes
        /// </summary>
        private void setValue()
        {
            _reservation.Insurance = txtInsurance.Text;
            _reservation.FacilityType = ddlFacilityType.Text;
            _reservation.FirstName = txtFname.Text.ToLower();
            _reservation.LastName = txtLName.Text.ToLower();
            _reservation.Phone = txtPhone.Text;
            _reservation.Email = txtEmail.Text.ToLower();
            _reservation.BasicRental = txtBasicRental.Text;
            _reservation.InsuranceAmount = txtInsuranceAmount.Text;
            _reservation.Taxes = txtTaxes.Text;
            _reservation.Total = txtTotal.Text;
            _reservation.FacilityAmount = Convert.ToDouble(txtFacility.Text.Trim());
        }

        /// <summary>
        /// sets default facility 
        /// </summary>
        private void setSelectedIndex()
        {
            ddlFacilityType.SelectedValue = string.Empty;
        }


        #endregion

    }

}