﻿#region "NameSpace"

using System;
using System.Data;

#endregion

namespace CarRentalApp
{
    /// <summary>
    /// This interface controls connection related operations with muSql
    /// </summary>
    public interface IConnectionManager
    {
        #region "Public Methods"

        /// <summary>
        /// Gets the selected columns for the query
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        DataSet getValue(String sqlCommand);

        /// <summary>
        /// Gets the single value for the query
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        int executeScalarQuery(String sqlCommand);

        /// <summary>
        /// Gets the insert/update count for the query
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        int executeNonQuery(String sqlCommand);

        #endregion
    }
}
