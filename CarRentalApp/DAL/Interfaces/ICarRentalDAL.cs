﻿#region "NameSpaces"

using CarRentalApp.BusinessLayer;
using System;
using System.Data;

#endregion

namespace CarRentalApp.DAL
{
    /// <summary>
    /// This interface handles DB realted operations
    /// </summary>
    public interface ICarRentalDAL
    {
        #region "Public Methods"

        /// <summary>
        /// Gets the list of reservations
        /// </summary>
        /// <returns></returns>
        DataSet getReservations();

        /// <summary>
        /// Updates Reservation
        /// </summary>
        /// <param name="reserve"></param>
        /// <returns></returns>
        int updateReservations(Reservation reserve);

        /// <summary>
        /// Get the drop down values based on the selected dropdown type
        /// </summary>
        /// <param name="selectors"></param>
        /// <returns></returns>
        DataSet getDropDownValues(string selectors);

        /// <summary>
        /// Validates location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        int validateLocation(string location);

        /// <summary>
        /// Validates facility
        /// </summary>
        /// <param name="facility"></param>
        /// <returns></returns>
        int validateFacility(String facility);

        /// <summary>
        /// Deletes the reservation
        /// </summary>
        /// <param name="resId"></param>
        /// <param name="cusId"></param>
        /// <returns></returns>
        int deleteReservation(string resId, string cusId);

        /// <summary>
        /// Creates Reservation
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        int reserveNow(Reservation reservation);

        /// <summary>
        /// Gets the list of reservations based on search criteria
        /// </summary>
        /// <param name="resId"></param>
        /// <param name="resDate"></param>
        /// <returns></returns>
        DataSet getReservationsBySearchCriteria(string resId, string cusId);

        /// <summary>
        /// Calulates Estimated Cost
        /// </summary>
        /// <param name="vehicleCategory"></param>
        /// <param name="facilityType"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        DataSet getEstimatedCosts(string vehicleCategory, string facilityType, string noOfDays);

        /// <summary>
        /// Gets the car category
        /// </summary>
        /// <param name="getCategory"></param>
        /// <returns></returns>
        DataSet getCategory(string getCategory);

        /// <summary>
        /// Generates Invoice
        /// </summary>
        /// <param name="reserveId"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        int generateInvoice(String reserveId, string duration);

        /// <summary>
        /// Gets the last inserted ReservationId 
        /// </summary>
        /// <returns></returns>
        int getReservationId();

        /// <summary>
        /// Get vehicle category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        DataSet getVehicleCategory(string category);

        /// <summary>
        /// check for valid user
        /// </summary>
        /// <param name="loginId">"UserName"</param>
        /// <param name="password">"Password"</param>
        /// <returns></returns>
        Boolean isValidUser(string loginId, string password);
    }

        #endregion    
}
