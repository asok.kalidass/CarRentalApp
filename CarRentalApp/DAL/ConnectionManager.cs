﻿#region "NameSpaces"

using System;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

#endregion

namespace CarRentalApp
{
    /// <summary>
    /// This class is responsible for handling connecgtions with MYSQL
    /// </summary>
    public class ConnectionManager : IConnectionManager
    {
        #region "Class Variables"

        MySqlConnection _connection;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ConnectionManager()
        {
            createConnection();
        }

        #endregion       

        #region "Public Methods"

        /// <summary>
        /// Gets the single value for the query
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        public int executeScalarQuery(String sqlCommand)
        {
            try
            {
                if (_connection.State == ConnectionState.Closed) this._connection.Open();
                MySqlCommand command = new MySqlCommand(sqlCommand, this._connection);
                return Convert.ToInt16(command.ExecuteScalar());
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the insert/update count for the query
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        public int executeNonQuery(String sqlCommand)
        {
            try
            { 
            if (_connection.State == ConnectionState.Closed) this._connection.Open();
            MySqlCommand command = new MySqlCommand(sqlCommand, this._connection);
            return Convert.ToInt16(command.ExecuteNonQuery());
        }
            catch(Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the selected columns for the query
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        public DataSet getValue(String sqlCommand)
        {
            try
            { 
            if (_connection.State == ConnectionState.Closed) this._connection.Open();
            MySqlCommand command = new MySqlCommand(sqlCommand, this._connection);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command);
            DataSet dataset = new DataSet();
            dataAdapter.Fill(dataset);
            closeConnection();
            return dataset;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region "Private Methods"

        /// <summary>
        /// Disposes the connection object
        /// </summary>
        private void closeConnection()
        {
            if (_connection.State == ConnectionState.Open)
            {
                this._connection.Close();
            }
        }

        /// <summary>
        /// Creates Connection
        /// </summary>
        private void createConnection()
        {
            this._connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["mySqlConnection"].ToString());
        }

        #endregion
    }
}
