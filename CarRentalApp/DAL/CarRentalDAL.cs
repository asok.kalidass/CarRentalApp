﻿#region "NameSpaces"

using CarRentalApp.BusinessLayer;
using System;
using System.Data;
using System.Text;

#endregion

namespace CarRentalApp.DAL
{
    /// <summary>
    /// This class handles all DB related handshake with mysql
    /// </summary>
    public class CarRentalDAL : ICarRentalDAL
    {
        #region "Instanse Variables"

        //connection manager instance variable
        IConnectionManager _database;

        StringBuilder _mySqlQuery;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        public CarRentalDAL()
        {            
            this._database = new ConnectionManager();         
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Get the drop down values based on the selected dropdown type
        /// </summary>
        /// <param name="selectors"></param>
        /// <returns></returns>
        public DataSet getDropDownValues(string selectors)
        {
            switch (selectors)
            {
                case "carCategory":
                    return _database.getValue("call get_CARCATEGORY();");
                case "locations":
                    return _database.getValue("call get_location();");
                case "facility":
                    return _database.getValue("call get_facilities();");
                default:
                    return _database.getValue("call get_age_set();");
            }                      
        }

        /// <summary>
        /// Gets the list of reservations
        /// </summary>
        /// <returns></returns>
        public DataSet getReservations()
        {          
            return _database.getValue("CALL get_reservations()");
        }

        /// <summary>
        /// Gets the list of reservations based on search criteria
        /// </summary>
        /// <param name="resId"></param>
        /// <param name="resDate"></param>
        /// <returns></returns>
        public DataSet getReservationsBySearchCriteria(string resId, string resDate)
        {
            return _database.getValue("CALL get_reservationsBySearchCriteria('" + resId + "', '" + resDate + "')");
        }

        /// <summary>
        /// Deletes the reservation
        /// </summary>
        /// <param name="resId"></param>
        /// <param name="cusId"></param>
        /// <returns></returns>
        public int deleteReservation(string resId, string cusId)
        {
            return _database.executeNonQuery("CALL delete_reservations('" + resId + "', '" + cusId + "')");
        }

        /// <summary>
        /// Generates Invoice
        /// </summary>
        /// <param name="reserveId"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        public int generateInvoice(String reserveId, string duration)
        {
            return _database.executeNonQuery("CALL generate_invoice('" + reserveId + "', '" + duration + "')");
        }

        /// <summary>
        /// Updates Reservation
        /// </summary>
        /// <param name="reserve"></param>
        /// <returns></returns>
        public int updateReservations(Reservation reserve)
        {
            this._mySqlQuery = new StringBuilder();
            _mySqlQuery.Append("update RESERVATION set ");           
            _mySqlQuery.Append("PICKUP_LOC = ");
            _mySqlQuery.Append(reserve.PickUpLocationId);
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("PICKUP_DATE = '");
            _mySqlQuery.Append(reserve.PickUpDate + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("PICKUP_TIME = '");
            _mySqlQuery.Append(reserve.PickUpTime + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("DROPOFF_LOC = ");
            _mySqlQuery.Append(reserve.DropOffLocationId);
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("DROPOFF_DATE = '");
            _mySqlQuery.Append(reserve.DropOffDate + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("DROPOFF_TIME = '");
            _mySqlQuery.Append(reserve.DropOffTime + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("FACILITY_ID = ");
            _mySqlQuery.Append("(select facility_id from facility where FACILITY_NAME = '");
            _mySqlQuery.Append(reserve.FacilityType + "')");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("BASIC_PRICE = '");
            _mySqlQuery.Append(reserve.BasicRental + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("INSUR_AMOUNT = '");
            _mySqlQuery.Append(reserve.InsuranceAmount + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("FACILITY_AMOUNT = '");
            _mySqlQuery.Append(reserve.FacilityAmount + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("TAX = '");
            _mySqlQuery.Append(reserve.Taxes + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("TOTAL_PRICE = '");
            _mySqlQuery.Append(reserve.Total + "'");
            _mySqlQuery.Append(", ");
            _mySqlQuery.Append("vin = '");
            _mySqlQuery.Append(reserve.Vin + "'");
            _mySqlQuery.Append(" where RES_ID = ");
            _mySqlQuery.Append(reserve.ReservationId);
            _mySqlQuery.Append(" and CUS_ID = ");
            _mySqlQuery.Append(reserve.CustomerId + ";");
            //set car availability to N
            _mySqlQuery.Append(" update car set avl_status = 'N'");
            _mySqlQuery.Append(" where vin = '");
            _mySqlQuery.Append(reserve.Vin + "';");
            return _database.executeNonQuery(_mySqlQuery.ToString());
        }

        /// <summary>
        /// Validates location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public int validateLocation(string location)
        {
            this._mySqlQuery = new StringBuilder();
            _mySqlQuery.Append("select Loc_id from location where loc_desc = '");
            _mySqlQuery.Append(location + "'");
            return _database.executeScalarQuery(_mySqlQuery.ToString());
        }

        /// <summary>
        /// Validates facility
        /// </summary>
        /// <param name="facility"></param>
        /// <returns></returns>
        public int validateFacility(String facility)
        {
            this._mySqlQuery = new StringBuilder();
            _mySqlQuery.Append("select facility_id from facility where facility_name = '");
            _mySqlQuery.Append(facility + "'");
            return _database.executeScalarQuery(_mySqlQuery.ToString());
        }

        /// <summary>
        /// Get vehicle category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public DataSet getVehicleCategory(string category)
        {
            return _database.getValue(" select cat_desc from car_category where cat_id = '" + category + "'");
        }

        /// <summary>
        /// Gets the last inserted ReservationId 
        /// </summary>
        /// <returns></returns>
        public int getReservationId()
        {
            this._mySqlQuery = new StringBuilder();
            _mySqlQuery.Append("select max(res_id) from reservation");
            return _database.executeScalarQuery(_mySqlQuery.ToString());
        }

        /// <summary>
        /// Calulates Estimated Cost
        /// </summary>
        /// <param name="vehicleCategory"></param>
        /// <param name="facilityType"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        public DataSet getEstimatedCosts(string vehicleCategory, string facilityType, string noOfDays)
        {
            return _database.getValue("call get_estimatedCost('" + vehicleCategory + "', '" + facilityType + "', '" + noOfDays + "');");
        }

        /// <summary>
        /// Gets the car category
        /// </summary>
        /// <param name="getCategory"></param>
        /// <returns></returns>
        public DataSet getCategory(string getCategory)
        {
            return _database.getValue("call check_car_availability('" + getCategory + "');");             
        }

        /// <summary>
        /// Creates Reservation
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        public int reserveNow(Reservation reservation)
        {
            this._mySqlQuery = new StringBuilder();
            //Insert into customer 
            _mySqlQuery.Append("INSERT INTO CUSTOMER(CUS_FNAME, CUS_LNAME, CUS_AGE_ID, CUS_EMAIL, CUS_PHONE) ");
            _mySqlQuery.Append(" VALUES('");
            _mySqlQuery.Append(reservation.FirstName.Trim() + "' , '");
            _mySqlQuery.Append(reservation.LastName.Trim() + "' , ");
            _mySqlQuery.Append(" (select age_id from age where age_set = '");
            _mySqlQuery.Append(reservation.Age + "'), '");
            _mySqlQuery.Append(reservation.Email.Trim() + "' , '");
            _mySqlQuery.Append(reservation.Phone.Trim() + "'); ");

            //Insert into reservation
            _mySqlQuery.Append("INSERT INTO RESERVATION(RES_DATE, PICKUP_LOC, ");
            _mySqlQuery.Append("PICKUP_DATE, PICKUP_TIME, DROPOFF_LOC, DROPOFF_DATE, DROPOFF_TIME, ");
            _mySqlQuery.Append("FACILITY_ID, BASIC_PRICE, INSUR_AMOUNT, TAX, FACILITY_AMOUNT, ");
            _mySqlQuery.Append("TOTAL_PRICE, CUS_ID, CAT_ID, curd_flag) ");
            _mySqlQuery.Append("VALUES('");
            _mySqlQuery.Append(System.DateTime.Today.ToShortDateString().ToString() + "' , '");
            _mySqlQuery.Append(reservation.PickUpLocationId.ToString() + "' , '");
            _mySqlQuery.Append(reservation.PickUpDate.Trim() + "' , '");
            _mySqlQuery.Append(reservation.PickUpTime.Trim() + "' , '");
            _mySqlQuery.Append(reservation.DropOffLocationId.ToString() + "' , '");
            _mySqlQuery.Append(reservation.DropOffDate.Trim() + "' , '");
            _mySqlQuery.Append(reservation.DropOffTime.Trim() + "' , ");
            _mySqlQuery.Append(reservation.FacilityId + ", '");
            _mySqlQuery.Append(reservation.BasicRental.Trim() + "' , '");
            _mySqlQuery.Append(reservation.InsuranceAmount.Trim() + "' , '");
            _mySqlQuery.Append(reservation.Taxes.Trim() + "' , '");
            _mySqlQuery.Append(reservation.FacilityAmount.ToString() + "' , '");
            _mySqlQuery.Append(reservation.Total.ToString() + "' , ");
            _mySqlQuery.Append(" (SELECT MAX(CUS_ID) FROM CUSTOMER) " + " , ");
            _mySqlQuery.Append(" (select cat_id from car_category where cat_desc = '");
           _mySqlQuery.Append(reservation.VehicleCategory.ToString() + "') , ");
            _mySqlQuery.Append("'I');");
            return _database.executeNonQuery(_mySqlQuery.ToString());
        }


        /// <summary>
        /// check for valid user
        /// </summary>
        /// <param name="loginId">"UserName"</param>
        /// <param name="password">"Password"</param>
        /// <returns></returns>
        public Boolean isValidUser(string loginId, string password)
        {
            return Convert.ToBoolean(_database.executeScalarQuery("call check_validUser(" + "'" + loginId + "'" + "," + "'" + password + "'" + ")"));
        }

        #endregion
    }
}
