﻿#region "NameSpaces"

using CarRentalApp.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This class manages operation performed on Reservation Screen
    /// </summary>
    public class RentalReserveManager : IRentalReserveManager
    {
        #region "Class Variables"

        private ICarRentalDAL _carRentalDAL;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        public RentalReserveManager()
        {
            _carRentalDAL = new CarRentalDAL();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Get DropDownValues
        /// </summary>
        /// <param name="selector">"drop down type"</param>
        /// <returns></returns>
        public IList<Entry> getDropDownValues(string selector)
        {
            int count = 1;

            List<Entry> keyValuePairs = new List<Entry>();
            DataSet dataset = _carRentalDAL.getDropDownValues(selector);
            keyValuePairs.Add(new Entry(0, ""));
            //Dataset to key and value pairs
            keyValuePairs.AddRange(dataset.Tables[0].AsEnumerable().Select(x => new Entry()
            {
                Id = count++,
                Value = x.Field<string>(0)
            }).ToList());           
            return keyValuePairs;
        }

        /// <summary>
        /// Gets Estimated Costs
        /// </summary>
        /// <param name="vehicleCategory"></param>
        /// <param name="facilityType"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        public IList<Entry> getEstimatedCosts(string vehicleCategory, string facilityType, string noOfDays)
        {
            int count = 1;

            List<Entry> keyValuePairs = new List<Entry>();
            DataSet dataset = _carRentalDAL.getEstimatedCosts(vehicleCategory, facilityType, noOfDays);
            //for each column in the select to list of type entry 
            for (int i = 0; i <= 4; i++)
            {
                keyValuePairs.AddRange(dataset.Tables[0].AsEnumerable().Select(x => new Entry()
                {
                    Id = count++,
                    Value = x.Field<Decimal>(i).ToString()
                }).ToList());
            }        
            return keyValuePairs;
        }

        /// <summary>
        /// Get Location Ids
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public int getLocationIds(String location)
        {
            return _carRentalDAL.validateLocation(location);
        }

        /// <summary>
        /// Get facility Id
        /// </summary>
        /// <param name="facility"></param>
        /// <returns></returns>
        public int getFacilityId(String facility)
        {
            return _carRentalDAL.validateFacility(facility);
        }

        /// <summary>
        /// Gernerates Reservation
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        public int reserveNow(Reservation reservation)
        {
            return _carRentalDAL.reserveNow(reservation);
        }

        /// <summary>
        /// Gets Reservation Id
        /// </summary>
        /// <returns></returns>
        public int getReservationId()
        {
            return _carRentalDAL.getReservationId();
        }

        #endregion
    }
}
