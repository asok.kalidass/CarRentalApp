﻿#region "NameSpaces"

using System;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This interface handles authorization of admin user 
    /// </summary>
    public interface IAuthorizationManager
    {
        #region "Public Methods"

        /// <summary>
        /// check for valid user
        /// </summary>
        /// <param name="loginId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Boolean isValidUser(string loginId, string password);

        #endregion
    }
}
