﻿#region "NameSpaces"

using System;
using System.Collections.Generic;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This interface performs operation in Reservation Screen
    /// </summary>
    public interface IRentalReserveManager
    {
        #region "Public Methods"

        /// <summary>
        /// Get DropDownValues
        /// </summary>
        /// <param name="selector">"drop down type"</param>
        /// <returns></returns>
        IList<Entry> getDropDownValues(string sqlQuery);

        /// <summary>
        /// Gets Estimated Costs
        /// </summary>
        /// <param name="vehicleCategory"></param>
        /// <param name="facilityType"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        IList<Entry> getEstimatedCosts(string vehicleCategory, string facilityType, string noOfDays);

        /// <summary>
        /// Gernerates Reservation
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        int reserveNow(Reservation reservation);

        /// <summary>
        /// Get Location Ids
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        int getLocationIds(String location);

        /// <summary>
        /// Get facility Id
        /// </summary>
        /// <param name="facility"></param>
        /// <returns></returns>
        int getFacilityId(String facility);

        /// <summary>
        /// Gets Reservation Id
        /// </summary>
        /// <returns></returns>
        int getReservationId();

        #endregion
    }
}
