﻿#region "NameSpaces"

using System.Collections.Generic;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This interface assists in getting the drop down values from mysql through DAL
    /// </summary>
    public interface IDropDownBinder
    {
        #region "Public Methods"

        /// <summary>
        /// Gets DropdownValues
        /// </summary>
        /// <param name="selector">"drop down type"</param>
        /// <returns></returns>
        IList<Entry> getDropDownValues(string sqlQuery);

        #endregion
    }
};
