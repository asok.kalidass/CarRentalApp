﻿#region "NameSpaces"

using System;
using System.Collections.Generic;
using System.Data;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This interface is responsible for IO operations in admin screen
    /// </summary>
    public interface IAdminManager
    {
        #region "Public Methods"

        /// <summary>
        /// Gets Reservation
        /// </summary>
        /// <returns></returns>
        DataSet getReservations();

        /// <summary>
        /// Updates Reservation
        /// </summary>
        /// <param name="reservation">""Reservation details</param>
        /// <returns></returns>
        int updateReservations(Reservation reservation);

        /// <summary>
        /// Validates Locations
        /// </summary>
        /// <param name="location">"Chosen location in FE"</param>
        /// <returns></returns>
        int validateLocation(String location);

        /// <summary>
        /// Validates Facility
        /// </summary>
        /// <param name="facility">"Chosen Facility"</param>
        /// <returns></returns>
        int validateFacility(String facility);

        /// <summary>
        /// Gets Reservation by search criteria
        /// </summary>
        /// <param name="resId">"Res Id"</param>
        /// <param name="resDate">"Reservation Data"</param>
        /// <returns></returns>
        DataSet getReservationsBySearchCriteria(string resId, string resDate);

        /// <summary>
        /// Deletes Reservation
        /// </summary>
        /// <param name="resId">"Reservation Id"</param>
        /// <param name="cusId">"Customer Id"</param>
        /// <returns></returns>
        int deleteReservation(string resId, string cusId);

        /// <summary>
        /// Gets Estimates Cost
        /// </summary>
        /// <param name="vehicleCategory"></param>
        /// <param name="facilityType"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        IList<Entry> getEstimatedCosts(string vehicleCategory, string facilityType, string noOfDays);

        /// <summary>
        /// Gets Vehicle Category based on selectors
        /// </summary>
        /// <param name="getCategory"></param>
        /// <returns></returns>
        IList<Entry> getCategory(string getCategory);

        /// <summary>
        /// Generates Invoice
        /// </summary>
        /// <param name="reserveId">"Reservation Id"</param>
        /// <param name="duration">"Number of days"</param>
        /// <returns></returns>
        int generateInvoice(String reserveId, string duration);

        /// <summary>
        /// Gets Vehicle Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        IList<Entry> getVehicleCategory(string category);

        #endregion
    }
}

