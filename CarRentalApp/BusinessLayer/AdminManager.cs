﻿#region "NameSpaces"

using CarRentalApp.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This class handles operations related to admin screen
    /// </summary>
    public class AdminManager : IAdminManager
    {
        #region "Class Variables"

        ICarRentalDAL _carRentalDAL;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        public AdminManager()
        {
            _carRentalDAL = new CarRentalDAL();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Gets Reservation
        /// </summary>
        /// <returns></returns>
        public DataSet getReservations()
        {
            return _carRentalDAL.getReservations();
        }

        /// <summary>
        /// Gets Reservation by search criteria
        /// </summary>
        /// <param name="resId">"Res Id"</param>
        /// <param name="resDate">"Reservation Data"</param>
        /// <returns></returns>
        public DataSet getReservationsBySearchCriteria(string resId, string resDate)
        {
            return _carRentalDAL.getReservationsBySearchCriteria(resId, resDate);
        }

        /// <summary>
        /// Updates Reservation
        /// </summary>
        /// <param name="reservation">""Reservation details</param>
        /// <returns></returns>
        public int updateReservations(Reservation reservation)
        {
            return _carRentalDAL.updateReservations(reservation);
        }

        /// <summary>
        /// Deletes Reservation
        /// </summary>
        /// <param name="resId">"Reservation Id"</param>
        /// <param name="cusId">"Customer Id"</param>
        /// <returns></returns>
        public int deleteReservation(string resId, string cusId)
        {
            return _carRentalDAL.deleteReservation(resId, cusId);
        }

        /// <summary>
        /// Validates Locations
        /// </summary>
        /// <param name="location">"Chosen location in FE"</param>
        /// <returns></returns>
        public int validateLocation(String location)
        {
            return _carRentalDAL.validateLocation(location);
        }

        /// <summary>
        /// Validates Facility
        /// </summary>
        /// <param name="facility">"Chosen Facility"</param>
        /// <returns></returns>
        public int validateFacility(String facility)
        {
            return _carRentalDAL.validateFacility(facility);
        }

        /// <summary>
        /// Generates Invoice
        /// </summary>
        /// <param name="reserveId">"Reservation Id"</param>
        /// <param name="duration">"Number of days"</param>
        /// <returns></returns>
        public int generateInvoice(String reserveId, string duration)
        {
            return _carRentalDAL.generateInvoice(reserveId, duration);
        }

        /// <summary>
        /// Gets Estimates Cost
        /// </summary>
        /// <param name="vehicleCategory"></param>
        /// <param name="facilityType"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        public IList<Entry> getEstimatedCosts(string vehicleCategory, string facilityType, string noOfDays)
        {
            int count = 1;

            List<Entry> keyValuePairs = new List<Entry>();
            //keyValuePairs.Add(new Entry(0, " "));
            DataSet dataset = _carRentalDAL.getEstimatedCosts(vehicleCategory, facilityType, noOfDays);
            for (int i = 0; i <= 4; i++)
            {
                keyValuePairs.AddRange(dataset.Tables[0].AsEnumerable().Select(x => new Entry()
                {
                    Id = count++,
                    Value = x.Field<Decimal>(i).ToString()
                }).ToList());
            }
            return keyValuePairs;
        }

        /// <summary>
        /// Gets Vehicle Category based on selectors
        /// </summary>
        /// <param name="getCategory"></param>
        /// <returns></returns>
        public IList<Entry> getCategory(string getCategory)
        {
            int count = 1;

            List<Entry> keyValuePairs = new List<Entry>();
            //keyValuePairs.Add(new Entry(0, " "));
            DataSet dataset = _carRentalDAL.getCategory(getCategory);
            for (int i = 0; i < 2; i++)
            {
                keyValuePairs.AddRange(dataset.Tables[0].AsEnumerable().Select(x => new Entry()
                {
                    Id = count++,
                    Value = x.Field<string>(i).ToString()
                }).ToList());
            }
            return keyValuePairs;
        }

        /// <summary>
        /// Gets Vehicle Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public IList<Entry> getVehicleCategory(string category)
        {
            int count = 1;

            List<Entry> keyValuePairs = new List<Entry>();
            //keyValuePairs.Add(new Entry(0, " "));
            DataSet dataset = _carRentalDAL.getVehicleCategory(category);
            {
                keyValuePairs.AddRange(dataset.Tables[0].AsEnumerable().Select(x => new Entry()
                {
                    Id = count++,
                    Value = x.Field<string>(0).ToString()
                }).ToList());
            }
            return keyValuePairs;
        }

        #endregion
    }
}
