﻿#region "NameSpaces"

using CarRentalApp.DAL;
using System;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This class validates the user to authorize for admin screen
    /// </summary>
    public class AuthorizationManager : IAuthorizationManager
    {
        #region "Class Variables"

        /// <summary>
        /// 
        /// </summary>
        ICarRentalDAL _carRentalDAL;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        public AuthorizationManager()
        {
            _carRentalDAL = new CarRentalDAL();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// check for valid user
        /// </summary>
        /// <param name="loginId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Boolean isValidUser(string loginId, string password)
        {
            return _carRentalDAL.isValidUser(loginId, password);
        }

        #endregion
    }
}
