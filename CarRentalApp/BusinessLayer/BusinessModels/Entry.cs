﻿namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This class gets the dropdown values as key value pair
    /// </summary>
    public class Entry
    {
        #region "Private Variables"

        private int _id;

        private string _value;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor to initialize with key and values
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public Entry(int id, string value)
        {
            this._id = id;
            this._value = value;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Entry()
        {

        }

        #endregion

        #region "Getters and Setters"

        /// <summary>
        /// Gets or Sets ID
        /// </summary>
        public int Id
        {
            get 
            {
                return _id;
            }

            set
                {
                _id = value;
            }
        }

        /// <summary>
        /// Gets or Sets Value
        /// </summary>
        public string Value
        {
            get
                {
                return _value;
            }
            set
                {
                _value = value;
            }
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Return String representation of this class
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Value;
        }

        #endregion
    }
}
