﻿namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This class reflects all the reservation details 
    /// </summary>
    public class Reservation
    {
        #region "Private Variables"

        private string _pickUpLocation;
        private string _dropOffLocation;
        private string _pickUpDate;
        private string _pickUpTime;
        private string _dropOffDate;
        private string _dropOffTime;
        private string _age;
        private string _vehicleCategory;
        private string _insurance;
        private string _facilityType;
        private string _firstName;
        private string _lastName;
        private string _phone;
        private string _email;
        private string _basicRental;
        private string _insuranceAmount;
        private string _taxes;
        private string _facility;
        private string _total;
        private int _facilityId;
        private int _customerId;
        private double _facilityAmount;
        private int _reservationId;
        private int _pickUpLocationId;
        private int _dropOffLocationId;
        private string _vin;
        private int _vehicleCategoryId;

        #endregion

        #region "Getters and Setters

        /// <summary>
        /// Gets or Sets PickUpLocation
        /// </summary>
        public string PickUpLocation
        {
            get
            {
                return _pickUpLocation;
            }

            set
            {
                _pickUpLocation = value;
            }
        }

        /// <summary>
        /// Gets or Sets PickUpLocationId
        /// </summary>
        public int PickUpLocationId
        {
            get
            {
                return _pickUpLocationId;
            }

            set
            {
                _pickUpLocationId = value;
            }
        }

        /// <summary>
        /// Gets or Sets DropOffLocationId
        /// </summary>
        public int DropOffLocationId
        {
            get
            {
                return _dropOffLocationId;
            }

            set
            {
                _dropOffLocationId = value;
            }
        }

        /// <summary>
        /// Gets or Sets DropOffLocation
        /// </summary>
        public string DropOffLocation
        {
            get
            {
                return _dropOffLocation;
            }

            set
            {
                _dropOffLocation = value;
            }
        }

        /// <summary>
        /// Gets or Sets PickUpDate
        /// </summary>
        public string PickUpDate
        {
            get
            {
                return _pickUpDate;
            }

            set
            {
                _pickUpDate = value;
            }
        }

        /// <summary>
        /// Gets or Sets PickUpTime
        /// </summary>
        public string PickUpTime
        {
            get
            {
                return _pickUpTime;
            }

            set
            {
                _pickUpTime = value;
            }
        }

        /// <summary>
        /// Gets or Sets DropOffDate
        /// </summary>
        public string DropOffDate
        {
            get
            {
                return _dropOffDate;
            }

            set
            {
                _dropOffDate = value;
            }
        }

        /// <summary>
        /// Gets or Sets DropOffTime
        /// </summary>
        public string DropOffTime
        {
            get
            {
                return _dropOffTime;
            }

            set
            {
                _dropOffTime = value;
            }
        }

        /// <summary>
        /// Gets or Sets Age
        /// </summary>
        public string Age
        {
            get
            {
                return _age;
            }

            set
            {
                _age = value;
            }
        }

        /// <summary>
        /// Gets or Sets VehicleCategory
        /// </summary>
        public string VehicleCategory
        {
            get
            {
                return _vehicleCategory;
            }

            set
            {
                _vehicleCategory = value;
            }
        }

        /// <summary>
        /// Gets or Sets Insurance
        /// </summary>
        public string Insurance
        {
            get
            {
                return _insurance;
            }

            set
            {
                _insurance = value;
            }
        }

        /// <summary>
        /// Gets or Sets FacilityType
        /// </summary>
        public string FacilityType
        {
            get
            {
                return _facilityType;
            }

            set
            {
                _facilityType = value;
            }
        }

        /// <summary>
        /// Gets or Sets FirstName
        /// </summary>
        public string FirstName
        {
            get
            {
                return _firstName;
            }

            set
            {
                _firstName = value;
            }
        }

        /// <summary>
        /// Gets or Sets LastName
        /// </summary>
        public string LastName
        {
            get
            {
                return _lastName;
            }

            set
            {
                _lastName = value;
            }
        }

        /// <summary>
        /// Gets or Sets Phone
        /// </summary>
        public string Phone
        {
            get
            {
                return _phone;
            }

            set
            {
                _phone = value;
            }
        }

        /// <summary>
        /// Gets or Sets Email
        /// </summary>
        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }

        /// <summary>
        /// Gets or Sets BasicRental
        /// </summary>
        public string BasicRental
        {
            get
            {
                return _basicRental;
            }

            set
            {
                _basicRental = value;
            }
        }

        /// <summary>
        /// Gets or Sets InsuranceAmount
        /// </summary>
        public string InsuranceAmount
        {
            get
            {
                return _insuranceAmount;
            }

            set
            {
                _insuranceAmount = value;
            }
        }

        /// <summary>
        /// Gets or Sets Taxes
        /// </summary>
        public string Taxes
        {
            get
            {
                return _taxes;
            }

            set
            {
                _taxes = value;
            }
        }

        /// <summary>
        /// Gets or Sets Facility
        /// </summary>
        public string Facility
        {
            get
            {
                return _facility;
            }

            set
            {
                _facility = value;
            }
        }

        /// <summary>
        /// Gets or Sets Total
        /// </summary>
        public string Total
        {
            get
            {
                return _total;
            }

            set
            {
                _total = value;
            }
        }

        /// <summary>
        /// Gets or Sets FacilityId
        /// </summary>
        public int FacilityId
        {
            get
            {
                return _facilityId;
            }

            set
            {
                _facilityId = value;
            }
        }

        /// <summary>
        /// Gets or Sets CustomerId
        /// </summary>
        public int CustomerId
        {
            get
            {
                return _customerId;
            }

            set
            {
                _customerId = value;
            }
        }

        /// <summary>
        /// Gets or Sets FacilityAmount
        /// </summary>
        public double FacilityAmount
        {
            get
            {
                return _facilityAmount;
            }

            set
            {
                _facilityAmount = value;
            }
        }

        /// <summary>
        /// Gets or Sets ReservationId
        /// </summary>
        public int ReservationId
        {
            get
            {
                return _reservationId;
            }

            set
            {
                _reservationId = value;
            }
        }

        /// <summary>
        /// Gets or Sets Vin
        /// </summary>
        public string Vin
        {
            get
            {
                return _vin;
            }

            set
            {
                _vin = value;
            }
        }

        /// <summary>
        /// Gets or Sets VehicleCategoryId
        /// </summary>
        public int VehicleCategoryId
        {
            get
            {
                return _vehicleCategoryId;
            }

            set
            {
                _vehicleCategoryId = value;
            }
        }

        #endregion
    }
}
