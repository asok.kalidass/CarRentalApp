﻿using CarRentalApp.DAL;
#region "NameSpaces"

using System.Collections.Generic;
using System.Data;
using System.Linq;

#endregion

namespace CarRentalApp.BusinessLayer
{
    /// <summary>
    /// This class is responsible for getting the dropdown values as key value pair
    /// </summary>
    public class DropDownBinder : IDropDownBinder
    {
        #region "Class Variables"

        //variable to aceess db methods
        ICarRentalDAL _carRentalDAL;

        #endregion

        #region "Constructor"

        /// <summary>
        /// Constructor
        /// </summary>
        public DropDownBinder()
        {
            _carRentalDAL = new CarRentalDAL();
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Gets DropdownValues
        /// </summary>
        /// <param name="selector">"drop down type"</param>
        /// <returns></returns>
        public IList<Entry> getDropDownValues(string selector)
        {
            int count = 0;

            IList<Entry> keyValuePairs = new List<Entry>();
            DataSet dataset = _carRentalDAL.getDropDownValues(selector);
            //convert the dataset into entry class type
            keyValuePairs = dataset.Tables[0].AsEnumerable().Select(x => new Entry()
            {
                Id = count++,
                Value = x.Field<string>(0)
            }).ToList();
            keyValuePairs.Add(new Entry(0, " "));
            return keyValuePairs;
        }

        #endregion
    }
}
