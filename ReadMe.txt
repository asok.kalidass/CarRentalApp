Description:

 The application handles reservation for a car rental company.
 Please refer to the Team16_User_manual for the business scope of the application.
 Please refer to the Team16_UTC_document for handling all the screens of the application. Also, find the validations incorporated at the end of UTC document only.


Pre requisites:

    Dot Net Framework 4 is needed to run this application.

    Please, replace the user id and password as that of your mysql workbench in CarRentalApp.exe file.

    Foe example, 
    
      <add name="mySqlConnection" connectionString="SERVER=localhost;DATABASE=teamcanadacarrental;UID=<yourUserId>;PASSWORD=<yourPassword>"/>

